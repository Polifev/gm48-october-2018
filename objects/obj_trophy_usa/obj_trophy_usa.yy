{
    "id": "9f784c5c-9ec0-4c0a-8408-3cc39ae23472",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trophy_usa",
    "eventList": [
        {
            "id": "289f1b89-dd9c-4087-934c-c9dcb193e261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f784c5c-9ec0-4c0a-8408-3cc39ae23472"
        },
        {
            "id": "653d8caa-5202-49b5-9130-36e69610e6a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9f784c5c-9ec0-4c0a-8408-3cc39ae23472"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fddd664-35a9-4664-b371-812fb4798659",
    "visible": true
}