{
    "id": "cbd08859-4a2a-4a52-93be-cb44d3942296",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_achievement_button",
    "eventList": [
        {
            "id": "92d00997-3b2b-420b-8ada-aa4db622f2ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbd08859-4a2a-4a52-93be-cb44d3942296"
        },
        {
            "id": "14ea387f-d077-4a5c-bacd-fbb2298ac4a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cbd08859-4a2a-4a52-93be-cb44d3942296"
        },
        {
            "id": "4982e498-451d-46e9-9e49-fff8409a22c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cbd08859-4a2a-4a52-93be-cb44d3942296"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
    "visible": true
}