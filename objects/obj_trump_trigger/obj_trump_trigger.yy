{
    "id": "17410c0d-6da2-4457-887b-ad3915bca477",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trump_trigger",
    "eventList": [
        {
            "id": "ca5ce8df-ae95-4065-bc0e-c1d20f32ca9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "17410c0d-6da2-4457-887b-ad3915bca477"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99ba2235-320c-4e4f-86f6-301dea8aa735",
    "visible": true
}