r = 16
ending = EndingUSA
if(point_in_circle(mouse_x, mouse_y,x+r, y+r, r)){
	audio_stop_sound(snd_main_music)
	audio_play_sound(snd_usa_song, 0, 0)
	obj_achievements.achievements[? "usa"] = true
	room_goto(EndingUSA)
}