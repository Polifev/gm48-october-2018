{
    "id": "d40d02a5-ea5c-4b02-b846-9159c1e45046",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clean_bar",
    "eventList": [
        {
            "id": "785c58fd-c572-4b89-9c91-b4810fa6da35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d40d02a5-ea5c-4b02-b846-9159c1e45046"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "821ed7b4-b027-4754-817a-3ca6bc52dd2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b86e187c-7020-4d66-9975-0d8028222bea",
    "visible": true
}