{
    "id": "0d59835a-1974-4012-a9b8-b8d34b8eb9d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drink_bar",
    "eventList": [
        {
            "id": "3d792935-9ef6-4a12-84f2-72c8da7eae47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d59835a-1974-4012-a9b8-b8d34b8eb9d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "821ed7b4-b027-4754-817a-3ca6bc52dd2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d831972a-8210-46d9-bada-c189e7ecfbea",
    "visible": true
}