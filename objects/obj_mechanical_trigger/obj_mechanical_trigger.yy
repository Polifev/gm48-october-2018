{
    "id": "88efe680-a2fe-4e47-814f-38747e41b68e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mechanical_trigger",
    "eventList": [
        {
            "id": "8a10c89a-b7b6-4ba8-9aaa-91cd821a8501",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "88efe680-a2fe-4e47-814f-38747e41b68e"
        },
        {
            "id": "c0192994-2ea1-4334-9831-30947e78e37e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "88efe680-a2fe-4e47-814f-38747e41b68e"
        },
        {
            "id": "6b6a27a0-5355-4fbf-ad86-e472b1e0ca7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "88efe680-a2fe-4e47-814f-38747e41b68e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a41e2d1a-812d-4ce7-9530-d9bfd5b8a1ff",
    "visible": false
}