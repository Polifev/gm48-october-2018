{
    "id": "70b58aec-9de9-4049-a357-53a0b9d3f93a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_creature_deco",
    "eventList": [
        {
            "id": "16b93a5b-c666-4449-a700-6aca49e3dc41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "70b58aec-9de9-4049-a357-53a0b9d3f93a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "09f76e30-f438-490e-9031-19671a575f72",
    "visible": true
}