{
    "id": "a57ad44a-22f0-4b6b-9df2-b9320fabd9f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_feed_button",
    "eventList": [
        {
            "id": "f166fae6-285e-486d-b02b-e6948cef4897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a57ad44a-22f0-4b6b-9df2-b9320fabd9f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cacd5eac-e736-41b9-bc66-491fb7c64d1a",
    "visible": true
}