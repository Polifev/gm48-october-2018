{
    "id": "1cc7595a-47bf-4091-b57a-6475fc021f0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enjoy_button",
    "eventList": [
        {
            "id": "15eb0c73-2ca0-4944-b5ba-667fb9924c0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1cc7595a-47bf-4091-b57a-6475fc021f0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "da51f8b2-1bce-4923-bece-71914e1656bf",
    "visible": true
}