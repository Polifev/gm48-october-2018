{
    "id": "17cc3c56-06a9-4e7e-ad9d-01c048a98c01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trophy_lab",
    "eventList": [
        {
            "id": "e8d6e577-b061-41e6-9ec0-db386f5ac0ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17cc3c56-06a9-4e7e-ad9d-01c048a98c01"
        },
        {
            "id": "cc8d9eb5-d94c-4db8-8ada-ae58c1f7c545",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "17cc3c56-06a9-4e7e-ad9d-01c048a98c01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fddd664-35a9-4664-b371-812fb4798659",
    "visible": true
}