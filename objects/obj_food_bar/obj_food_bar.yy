{
    "id": "7c131d36-ad23-4dae-acfe-76aada96e86b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_food_bar",
    "eventList": [
        {
            "id": "323b5276-bb43-4bf9-8327-d96ffbedfe6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c131d36-ad23-4dae-acfe-76aada96e86b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "821ed7b4-b027-4754-817a-3ca6bc52dd2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b7b51039-f35c-43cc-9645-657236758e7a",
    "visible": true
}