/// @description
switch(state){
	case CreatureState.HAPPY: break;	
	case CreatureState.ASLEEP: break;	
	case CreatureState.SAD: break;	
	case CreatureState.EATING: audio_play_sound(snd_eat, 1, 0) break;
	case CreatureState.DRINKING: audio_play_sound(snd_drink, 1, 0) break;
	case CreatureState.ENJOYING: break;
	case CreatureState.CLEANING: break;
	case CreatureState.DEAD: break;
	default: break;
}
alarm_set(0, 15)
