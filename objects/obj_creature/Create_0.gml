/// @description Initialize variables
// You can write your code in this editor

enum CreatureState{
	HAPPY,
	ASLEEP,
	SAD,
	DRINKING,
	EATING,
	ENJOYING,
	CLEANING,
	DEAD
}
anchor_x = x;
anchor_y = y;

bars[0] = obj_food_bar
bars[1] = obj_drink_bar
bars[2] = obj_enjoy_bar
bars[3] = obj_sleep_bar
bars[4] = obj_clean_bar

decrease_rate[0] = 0.001
decrease_rate[1] = 0.001
decrease_rate[2] = 0.0005
decrease_rate[3] = 0.001
decrease_rate[4] = 0.0005

state = CreatureState.HAPPY
time_in_current_state = 0

timeEllapsedInStateHappy = 0;
timeEllapsedInStateSad = 0;

overeating = 0

alarm_set(0, 15)