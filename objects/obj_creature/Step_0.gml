/// @description Update statistics
for(i = 0; i < 5; i++){
	bars[i].value = min(1, max(0, bars[i].value - decrease_rate[i]))
}
switch(state){
	case CreatureState.HAPPY: scr_happy(); break;	
	case CreatureState.ASLEEP: scr_asleep(); break;	
	case CreatureState.SAD: scr_sad(); break;	
	case CreatureState.EATING: scr_eating(); break;
	case CreatureState.DRINKING: scr_drinking(); break;
	case CreatureState.ENJOYING: scr_enjoying(); break;
	case CreatureState.CLEANING: scr_cleaning(); break;
	case CreatureState.DEAD: scr_dead(); break;
	default: break;
}
if(bars[3].value == 0 && state != CreatureState.DEAD){
	x = anchor_x;
	y = anchor_y;
	state = CreatureState.ASLEEP;
}

if((bars[0].value==0 || bars[1].value==0) && state != CreatureState.DEAD){
	instance_create_layer(120, 68, "GameOver", obj_gameover)
	with(obj_achievements){
		ds_map_replace(achievements, "dontstarve", true) 
	}
	audio_stop_all();
	audio_play_sound(snd_lose, 0, 0)
	obj_gameover.message = "You need to feed your creature"
	state = CreatureState.DEAD
}

if((bars[2].value==0) && state != CreatureState.DEAD){
	with(obj_achievements){
		ds_map_replace(achievements, "skyrim", true) 
	}
	room_goto(EndingSkyrim)
}

if((bars[4].value==0) && state != CreatureState.DEAD){
	with(obj_achievements){
		ds_map_replace(achievements, "time", true) 
	}
	room_goto(EndingTime)
}
