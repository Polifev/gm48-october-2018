{
    "id": "71d22a72-2355-4843-b381-cbf934098861",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_creature",
    "eventList": [
        {
            "id": "30efc312-65dd-49d1-90fe-432f1241c554",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71d22a72-2355-4843-b381-cbf934098861"
        },
        {
            "id": "8831c423-52c4-4c83-8dd0-e427c1c1a345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71d22a72-2355-4843-b381-cbf934098861"
        },
        {
            "id": "6a471a57-f07a-4194-bb9d-436647bb1dd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "71d22a72-2355-4843-b381-cbf934098861"
        },
        {
            "id": "b99a0a90-835f-426f-8747-dfd5791d10cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "71d22a72-2355-4843-b381-cbf934098861"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e13291cb-43ec-4ff7-9610-3e61b56f1cfa",
    "visible": true
}