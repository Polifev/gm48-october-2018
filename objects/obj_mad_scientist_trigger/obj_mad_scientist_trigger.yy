{
    "id": "eb9c9fa8-46f5-423b-9373-6a9bbb09b3a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mad_scientist_trigger",
    "eventList": [
        {
            "id": "140a46c5-af44-4f2a-be26-8a2696dd1718",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "eb9c9fa8-46f5-423b-9373-6a9bbb09b3a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca4373c5-d725-49ee-b009-41ed5354fcef",
    "visible": true
}