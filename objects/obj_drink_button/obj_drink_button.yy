{
    "id": "153af425-9272-4fe7-a615-f20bce01fd1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drink_button",
    "eventList": [
        {
            "id": "6e970453-c69a-4c1c-83c9-ed4a7b9e2814",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "153af425-9272-4fe7-a615-f20bce01fd1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "63a80963-b503-4c46-b8c9-6092cd927037",
    "visible": true
}