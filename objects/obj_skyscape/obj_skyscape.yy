{
    "id": "4e47e639-e2ec-4076-9304-d9be6124d26b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_skyscape",
    "eventList": [
        {
            "id": "c669725b-1a1f-406c-ac53-fa67eacd55c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e47e639-e2ec-4076-9304-d9be6124d26b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d019a74f-36cb-4de5-b1f1-2fe29d291259",
    "visible": true
}