/// @description Reset clicks buffer
if(clicks > 10){
	audio_stop_all()
	audio_play_sound(snd_bug, 0, 1)
	room_goto(EndingBug)
	with(obj_achievements){
		ds_map_replace(achievements, "bug", true)
	}
}
else{
	clicks = max(0, clicks - 1)
	alarm_set(0, 30)
}
