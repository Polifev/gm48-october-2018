{
    "id": "0211ffda-7d69-4a39-bf16-87629da230b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ingame_datas",
    "eventList": [
        {
            "id": "a0cd09a7-cab6-4b90-83e3-a388e98616ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0211ffda-7d69-4a39-bf16-87629da230b2"
        },
        {
            "id": "a0a36fa3-b2e3-4b2d-923d-1b3d76076e3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0211ffda-7d69-4a39-bf16-87629da230b2"
        },
        {
            "id": "9d26c7d4-d26d-4d75-9555-32d7c6fb035e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0211ffda-7d69-4a39-bf16-87629da230b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}