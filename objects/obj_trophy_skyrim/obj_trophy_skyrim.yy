{
    "id": "89d9f489-60da-493d-b869-4a79bb25d8e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trophy_skyrim",
    "eventList": [
        {
            "id": "faac36d9-6dd1-43fa-9ce7-70fc4572a403",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89d9f489-60da-493d-b869-4a79bb25d8e6"
        },
        {
            "id": "d36cc29b-e70b-48f0-b2ef-9c48bcad5cfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "89d9f489-60da-493d-b869-4a79bb25d8e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fddd664-35a9-4664-b371-812fb4798659",
    "visible": true
}