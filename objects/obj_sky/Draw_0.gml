var hour = date_get_hour( obj_ingame_datas.ingame_date ) + date_get_minute( obj_ingame_datas.ingame_date ) / 60
var r, g, b;

if(hour < 5){
	r = color_get_red(col_dawn) * hour + color_get_red(col_night) * (5 - hour)
	r /= 5
	
	g = color_get_green(col_dawn) * hour + color_get_green(col_night) * (5 - hour)
	g /= 5
	
	b = color_get_blue(col_dawn) * hour + color_get_blue(col_night) * (5 - hour)
	b /= 5
}
else if(hour < 12){
	r = color_get_red(col_day) * (hour-5) + color_get_red(col_dawn) * (12 - hour)
	r /= 7
	
	g = color_get_green(col_day) * (hour-5) + color_get_green(col_dawn) * (12 - hour)
	g /= 7
	
	b = color_get_blue(col_day) * (hour-5) + color_get_blue(col_dawn) * (12 - hour)
	b /= 7
}
else if(hour < 20){
	r = color_get_red(col_twilight) * (hour-12) + color_get_red(col_day) * (20 - hour)
	r /= 8
	
	g = color_get_green(col_twilight) * (hour-12) + color_get_green(col_day) * (20 - hour)
	g /= 8
	
	b = color_get_blue(col_twilight) * (hour-12) + color_get_blue(col_day) * (20 - hour)
	b /= 8
}
else{
	r = color_get_red(col_night) * (hour-20) + color_get_red(col_twilight) * (24 - hour)
	r /= 4
	
	g = color_get_green(col_night) * (hour-20) + color_get_green(col_twilight) * (24 - hour)
	g /= 4
	
	b = color_get_blue(col_night) * (hour-20) + color_get_blue(col_twilight) * (24 - hour)
	b /= 4
}

blend_color = make_color_rgb(r,g,b);
draw_sprite_ext(sprite_index, image_index, x, y, 1, 1, 0, blend_color, 1);
draw_text(100, 100, string(r))