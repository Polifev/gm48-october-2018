{
    "id": "f6ff875b-e05f-4058-9960-1677fa85d19d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sky",
    "eventList": [
        {
            "id": "76af2d1d-10cd-43fb-8750-b7a814af3961",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6ff875b-e05f-4058-9960-1677fa85d19d"
        },
        {
            "id": "9be287f5-96f8-400c-bf2f-a6751c4f09be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f6ff875b-e05f-4058-9960-1677fa85d19d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93cb53f7-9a96-4526-aac4-6d5613a2fe41",
    "visible": true
}