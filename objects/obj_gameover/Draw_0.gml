/// @description Draw gameOver

draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_font(fon_h1)
draw_text(x, y, "Game Over")
draw_set_font(fon_h2);
draw_set_color(c_white)
draw_text(x+1, y+17, message)
draw_text(x+1, y+34, "press <enter> to go back to menu")
draw_set_color(c_black)
draw_text(x, y+17, message)
draw_text(x, y+34, "press <enter> to go back to menu")
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_color(c_white);