/// @description Insert Go back to main menu
room_goto(MainMenu)
with(obj_music_manager)
{
	audio_stop_all()
	audio_play_sound(snd_menu, 0, 1)
}