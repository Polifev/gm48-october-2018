{
    "id": "94e34b43-6394-4270-958d-8db818ea10cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameover",
    "eventList": [
        {
            "id": "7bb15009-0860-4c35-962f-ec23d01b5e6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "94e34b43-6394-4270-958d-8db818ea10cf"
        },
        {
            "id": "85bbb4d6-3eab-4a05-9a6c-557d886fd071",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "94e34b43-6394-4270-958d-8db818ea10cf"
        },
        {
            "id": "55c82c13-d15f-42a2-8a83-936c8ff0ee80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94e34b43-6394-4270-958d-8db818ea10cf"
        },
        {
            "id": "de1f6750-0056-4d82-a7e1-db5af28e58b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "94e34b43-6394-4270-958d-8db818ea10cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}