/// @description Initialize options and load achievements

achievements = ds_map_create()
ds_map_add(achievements, "dontstarve", false) // Mourir de faim
ds_map_add(achievements, "usa", false) // Gagner les élections
ds_map_add(achievements, "fat", false) // Manger/boire alors qu'il n'a pas faim/soif
ds_map_add(achievements, "24PM", false) // Manger/boire après minuit
ds_map_add(achievements, "bug", false) // masher
ds_map_add(achievements, "skyrim", false) // skyrim
ds_map_add(achievements, "lab", false) // skyrim
ds_map_add(achievements, "time", false) // skyrim
