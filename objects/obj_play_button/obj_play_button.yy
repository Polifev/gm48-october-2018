{
    "id": "140fb0b1-feb5-4180-a07c-18404f4ad897",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_play_button",
    "eventList": [
        {
            "id": "b750702a-617b-4d60-8ff9-4bb82896ba43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "140fb0b1-feb5-4180-a07c-18404f4ad897"
        },
        {
            "id": "f81da440-0bae-43b7-aa8c-bd072a53a33e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "140fb0b1-feb5-4180-a07c-18404f4ad897"
        },
        {
            "id": "ddceedc5-f334-4496-a1fe-5588c5e28287",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "140fb0b1-feb5-4180-a07c-18404f4ad897"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
    "visible": true
}