{
    "id": "767427fa-e66e-4f1a-9c17-86275b3c8f52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enjoy_bar",
    "eventList": [
        {
            "id": "77bc47b6-7dda-448a-9b2f-909be87abbd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "767427fa-e66e-4f1a-9c17-86275b3c8f52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "821ed7b4-b027-4754-817a-3ca6bc52dd2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d5e3f06-6b7b-4d40-9301-1dee43ad6bd1",
    "visible": true
}