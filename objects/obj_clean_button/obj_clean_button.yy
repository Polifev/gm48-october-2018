{
    "id": "9bb0c3e9-a4e0-43d4-b92d-cd50ffe6065f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clean_button",
    "eventList": [
        {
            "id": "901b7cc3-cdd4-42e9-a532-dd138890d5a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9bb0c3e9-a4e0-43d4-b92d-cd50ffe6065f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3841af53-f124-4735-86a2-8c2171f0a933",
    "visible": true
}