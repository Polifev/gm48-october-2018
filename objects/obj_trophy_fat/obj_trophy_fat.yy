{
    "id": "d44e8ca2-ea7f-4065-88f9-01366fb25474",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trophy_fat",
    "eventList": [
        {
            "id": "7844a3aa-cfc8-4106-acd0-f094f1715482",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d44e8ca2-ea7f-4065-88f9-01366fb25474"
        },
        {
            "id": "b34f9515-bcaf-4214-b763-4c652b7a2957",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d44e8ca2-ea7f-4065-88f9-01366fb25474"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fddd664-35a9-4664-b371-812fb4798659",
    "visible": true
}