{
    "id": "db715fb7-8d50-4400-8f66-bf3ad84733e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trophy_gremlin",
    "eventList": [
        {
            "id": "e7ec9495-4124-4ddd-a7d2-33a455376ec8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db715fb7-8d50-4400-8f66-bf3ad84733e1"
        },
        {
            "id": "52c0e2fb-50a0-4ef3-8dbb-f641f3eb7fe5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "db715fb7-8d50-4400-8f66-bf3ad84733e1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fddd664-35a9-4664-b371-812fb4798659",
    "visible": true
}