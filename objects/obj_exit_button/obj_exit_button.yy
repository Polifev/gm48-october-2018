{
    "id": "8bf8e2ef-28ab-401f-b5c6-b25069c8e431",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exit_button",
    "eventList": [
        {
            "id": "e47f33c3-65a3-4048-a1ab-ed49781e6c7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bf8e2ef-28ab-401f-b5c6-b25069c8e431"
        },
        {
            "id": "3617557a-72a6-4bd8-a040-b18730f293ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8bf8e2ef-28ab-401f-b5c6-b25069c8e431"
        },
        {
            "id": "cb9db732-2d92-4e5c-9a4a-1fdddedff389",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "8bf8e2ef-28ab-401f-b5c6-b25069c8e431"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
    "visible": true
}