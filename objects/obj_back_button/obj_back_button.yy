{
    "id": "a4239df9-d50b-410c-969e-1acd48575612",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_back_button",
    "eventList": [
        {
            "id": "fb175d6f-b9bf-4918-b309-ba5bf9375b63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a4239df9-d50b-410c-969e-1acd48575612"
        },
        {
            "id": "92e380a6-6a81-4c65-a493-eb2b53655067",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a4239df9-d50b-410c-969e-1acd48575612"
        },
        {
            "id": "69ec8e50-873d-4e74-937d-f5161063cc10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a4239df9-d50b-410c-969e-1acd48575612"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e50b8e0c-72e0-4f1f-9a49-9d3465784636",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
    "visible": true
}