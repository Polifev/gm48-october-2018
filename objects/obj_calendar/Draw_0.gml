/// @description Draw dateTime
draw_self()
var offset = 5
if(date_get_day(obj_ingame_datas.ingame_date) < 10)
{
	offset = 4
}
str = date_date_string(obj_ingame_datas.ingame_date) + "  " + string_copy(date_time_string(obj_ingame_datas.ingame_date), 0, 5)
draw_set_font(fon_main)
draw_text(x+4, y+2, str)