{
    "id": "c4eee82f-986e-484b-a14a-13220136540e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_calendar",
    "eventList": [
        {
            "id": "d97e22a7-52a4-47a7-8ed2-b2c67e4c1162",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c4eee82f-986e-484b-a14a-13220136540e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6fe81945-4370-4fe7-be76-8ab4aaaffe60",
    "visible": true
}