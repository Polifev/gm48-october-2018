{
    "id": "634f64ae-de53-4699-874f-b28b241413ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mcd",
    "eventList": [
        {
            "id": "219c84e0-5157-4379-8679-aa5e4bc39ca0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "634f64ae-de53-4699-874f-b28b241413ae"
        },
        {
            "id": "c37f9488-0c7a-43b4-a77c-f3086145d38d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "634f64ae-de53-4699-874f-b28b241413ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5de9edda-8d8e-4535-8b06-2c10b213e03e",
    "visible": true
}