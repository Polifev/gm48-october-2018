/// @description Draw the bar and its content
draw_set_color(bar_color)
draw_rectangle(x + x_offset, y + y_offset, x + x_offset + (value * bar_length), y + y_offset + bar_height, 0)
draw_set_color(c_white)
draw_self()

