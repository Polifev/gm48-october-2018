{
    "id": "8edc5513-3004-4228-90d9-1d28eb1e35b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sleep_bar",
    "eventList": [
        {
            "id": "cdc19abc-8b33-4676-90f7-db5194d4a595",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8edc5513-3004-4228-90d9-1d28eb1e35b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "821ed7b4-b027-4754-817a-3ca6bc52dd2e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "05f8a2d2-c507-4bef-8c9e-48d51a0e968b",
    "visible": true
}