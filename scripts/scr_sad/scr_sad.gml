with(obj_creature)
{	
	decrease_rate[0] = 0.003
	decrease_rate[1] = 0.003
	decrease_rate[2] = 0.0005
	decrease_rate[3] = 0.003
	decrease_rate[4] = 0.0005
	sprite_index = spr_creature_sad

	if(bars[0].value >= 0.7 && bars[1].value >= 0.7){
		state = CreatureState.HAPPY
	}
}

