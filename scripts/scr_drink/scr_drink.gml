with obj_drink_bar
{
	if( obj_creature.state != CreatureState.ASLEEP && obj_creature.state != CreatureState.DEAD ){
		obj_creature.x = obj_creature.anchor_x
		obj_creature.y = obj_creature.anchor_y
		if( value > 0.8 && obj_creature.state != CreatureState.DRINKING){
			obj_creature.overeating ++
			if(obj_creature.overeating > 10){
				room_goto(EndingFat)
				with(obj_achievements){
					ds_map_replace(achievements, "fat", true) 
				}
			}
		}
		obj_creature.state = CreatureState.DRINKING;
	}
}

obj_ingame_datas.clicks ++;