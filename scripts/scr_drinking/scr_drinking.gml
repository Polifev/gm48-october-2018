with(obj_creature)
{	
	decrease_rate[0] = 0.001
	decrease_rate[1] = -0.005
	decrease_rate[2] = 0.002
	decrease_rate[3] = 0.001
	decrease_rate[4] = 0.001
	sprite_index = spr_creature_drinking
	if(bars[1].value == 1){
		if(bars[0].value >= 0.3){
			state = CreatureState.HAPPY
		}else{
			state = CreatureState.SAD
		}
	}
}