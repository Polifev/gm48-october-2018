with(obj_creature)
{	
	decrease_rate[0] = 0.001
	decrease_rate[1] = 0.001
	decrease_rate[2] = -0.005
	decrease_rate[3] = 0.003
	decrease_rate[4] = 0.001
	sprite_index = spr_creature_enjoying
	x = anchor_x - 64;
	y = anchor_y;

	if(bars[2].value == 1){
		x = anchor_x;
		y = anchor_y;
		
		if(bars[0].value >= 0.3 && bars[1].value >= 0.3){
			state = CreatureState.HAPPY
		}else{
			state = CreatureState.SAD
		}
	}
}