with(obj_creature)
{	
	decrease_rate[0] = -0.005
	decrease_rate[1] = 0.001
	decrease_rate[2] = 0.002
	decrease_rate[3] = 0.001
	decrease_rate[4] = 0.001
	sprite_index = spr_creature_eating

	if(date_get_hour( obj_ingame_datas.ingame_date ) < 1){
		with(obj_achievements){
		ds_map_replace(achievements, "24PM", true) 
		}
		audio_stop_sound(snd_main_music)
		audio_play_sound(snd_creepy_song, 0, 1)
		room_goto(EndingGremlin)
	}

	if(bars[0].value == 1){
		if(bars[1].value >= 0.3){
			state = CreatureState.HAPPY
		}else{
			state = CreatureState.SAD
		}
	}
}