with(obj_creature)
{	
	decrease_rate[0] = 0.001
	decrease_rate[1] = 0.001
	decrease_rate[2] = 0.001
	decrease_rate[3] = 0.0005
	decrease_rate[4] = -0.005
	sprite_index = spr_creature_cleaning

	if(bars[4].value == 1){
		x = anchor_x;
		y = anchor_y;
		
		if(bars[0].value >= 0.3 && bars[1].value >= 0.3){
			state = CreatureState.HAPPY
		}else{
			state = CreatureState.SAD
		}
	}
}