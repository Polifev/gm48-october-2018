with(obj_creature)
{
	decrease_rate[0] = 0.001;
	decrease_rate[1] = 0.001;
	decrease_rate[2] = 0.0005;
	decrease_rate[3] = 0.001;
	decrease_rate[4] = 0.0005;
	sprite_index = spr_creature_happy;

	if(bars[0].value < 0.4 || bars[1].value < 0.4){
		state = CreatureState.SAD;
	}
}