{
    "id": "f6ab839b-5392-4f53-a7f7-6b052c0068b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_old",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c654ce5-f6a4-4e69-b53c-9eb783d26625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6ab839b-5392-4f53-a7f7-6b052c0068b7",
            "compositeImage": {
                "id": "fda2a681-8230-4f37-8069-d681cfdc66c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c654ce5-f6a4-4e69-b53c-9eb783d26625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c21b77-4258-4f67-b453-5ebb30be3b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c654ce5-f6a4-4e69-b53c-9eb783d26625",
                    "LayerId": "10b6ec74-40b9-4c02-9c37-d0a4fadeb034"
                }
            ]
        },
        {
            "id": "9fbf1f00-778f-4b72-9b21-2b88e29fe977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6ab839b-5392-4f53-a7f7-6b052c0068b7",
            "compositeImage": {
                "id": "af026540-1f0f-413d-8747-9c3797a9bbc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbf1f00-778f-4b72-9b21-2b88e29fe977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb1940a-38b9-4d91-a1d6-da1c0999eb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbf1f00-778f-4b72-9b21-2b88e29fe977",
                    "LayerId": "10b6ec74-40b9-4c02-9c37-d0a4fadeb034"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "10b6ec74-40b9-4c02-9c37-d0a4fadeb034",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6ab839b-5392-4f53-a7f7-6b052c0068b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}