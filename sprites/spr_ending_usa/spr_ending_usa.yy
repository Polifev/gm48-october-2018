{
    "id": "b71d0bfe-91c9-434b-b663-0df69950a560",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_usa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d327e6ed-deb5-4f90-ac6a-cdc6398694d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71d0bfe-91c9-434b-b663-0df69950a560",
            "compositeImage": {
                "id": "8828de50-bc11-439c-af5b-6bb601f480dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d327e6ed-deb5-4f90-ac6a-cdc6398694d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de144291-6245-4cfb-91ad-550ce7366704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d327e6ed-deb5-4f90-ac6a-cdc6398694d8",
                    "LayerId": "37587d6b-433d-4e03-8a0b-4ca7982fb1e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "37587d6b-433d-4e03-8a0b-4ca7982fb1e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b71d0bfe-91c9-434b-b663-0df69950a560",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}