{
    "id": "fbc514c9-1152-45c1-9f79-382f77eeaf1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trophies",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "824d6d63-5f75-4c3a-841d-af71cd28bedb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc514c9-1152-45c1-9f79-382f77eeaf1c",
            "compositeImage": {
                "id": "c474bb2b-9c6f-4d61-9da7-3bb157bb77bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "824d6d63-5f75-4c3a-841d-af71cd28bedb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2685292-782d-4920-94ba-4be727888453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "824d6d63-5f75-4c3a-841d-af71cd28bedb",
                    "LayerId": "1b1a05d0-3349-4bc4-8dde-0376432b699c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "1b1a05d0-3349-4bc4-8dde-0376432b699c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbc514c9-1152-45c1-9f79-382f77eeaf1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}