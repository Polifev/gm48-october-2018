{
    "id": "e13291cb-43ec-4ff7-9610-3e61b56f1cfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_happy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6780f5d-e4e5-41e7-b82d-8c3860e0258e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e13291cb-43ec-4ff7-9610-3e61b56f1cfa",
            "compositeImage": {
                "id": "b5171e0d-0dc4-45fe-8f52-234a1b32427c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6780f5d-e4e5-41e7-b82d-8c3860e0258e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c162d3-fffe-4bfc-ac23-5876244a833e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6780f5d-e4e5-41e7-b82d-8c3860e0258e",
                    "LayerId": "586951f7-79b8-4f52-9ac3-f624be346e95"
                }
            ]
        },
        {
            "id": "47cf1c0b-82b8-4df7-a718-7254c38a6628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e13291cb-43ec-4ff7-9610-3e61b56f1cfa",
            "compositeImage": {
                "id": "77df7480-1f11-45cc-8e50-ceb34929b5a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47cf1c0b-82b8-4df7-a718-7254c38a6628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae61cf96-6de6-45fd-b564-18f1919327f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47cf1c0b-82b8-4df7-a718-7254c38a6628",
                    "LayerId": "586951f7-79b8-4f52-9ac3-f624be346e95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "586951f7-79b8-4f52-9ac3-f624be346e95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e13291cb-43ec-4ff7-9610-3e61b56f1cfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}