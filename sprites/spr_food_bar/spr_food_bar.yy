{
    "id": "b7b51039-f35c-43cc-9645-657236758e7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_food_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a41fcf08-672d-4184-9e7d-54e805803618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7b51039-f35c-43cc-9645-657236758e7a",
            "compositeImage": {
                "id": "2fc857fc-e81a-47c8-a1ac-e20c0cc8956c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41fcf08-672d-4184-9e7d-54e805803618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6773ee-394a-4a41-9222-1842dcb77614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41fcf08-672d-4184-9e7d-54e805803618",
                    "LayerId": "ae16b34e-3649-47f8-9f7a-3c72550a37a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ae16b34e-3649-47f8-9f7a-3c72550a37a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7b51039-f35c-43cc-9645-657236758e7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}