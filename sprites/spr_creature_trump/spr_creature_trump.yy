{
    "id": "4f17575b-1a55-4ea2-b5d5-53ab09a2c61e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_trump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04dc3b19-9265-4010-8542-bbcafd74e2ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f17575b-1a55-4ea2-b5d5-53ab09a2c61e",
            "compositeImage": {
                "id": "c7fb9f10-446b-4c4a-b9f2-8038c15ccca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04dc3b19-9265-4010-8542-bbcafd74e2ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea3a6f9-526e-4fca-9e39-2924134c7759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04dc3b19-9265-4010-8542-bbcafd74e2ed",
                    "LayerId": "0e462be8-a734-4baa-879a-da5eb4c3f218"
                }
            ]
        },
        {
            "id": "b09ffa0b-c4e8-4b52-b5cb-64c919a42d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f17575b-1a55-4ea2-b5d5-53ab09a2c61e",
            "compositeImage": {
                "id": "5e6121e5-fcb4-4b91-ae98-d9949df032ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b09ffa0b-c4e8-4b52-b5cb-64c919a42d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d4b73f-7d00-414e-b41b-f0ec0a8885a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b09ffa0b-c4e8-4b52-b5cb-64c919a42d61",
                    "LayerId": "0e462be8-a734-4baa-879a-da5eb4c3f218"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0e462be8-a734-4baa-879a-da5eb4c3f218",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f17575b-1a55-4ea2-b5d5-53ab09a2c61e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}