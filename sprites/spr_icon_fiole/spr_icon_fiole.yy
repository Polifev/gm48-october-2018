{
    "id": "9d1254c8-55cc-4751-a39c-78665632c121",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_fiole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ebd1b56-b667-409b-abaf-75599c69ea63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d1254c8-55cc-4751-a39c-78665632c121",
            "compositeImage": {
                "id": "bfa1d099-9162-4553-8939-9937c1eaf941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ebd1b56-b667-409b-abaf-75599c69ea63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f0571b-73a8-4f9f-96ed-41de2f73da22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ebd1b56-b667-409b-abaf-75599c69ea63",
                    "LayerId": "9e7d1f4e-4708-47e8-a794-486381fd1466"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e7d1f4e-4708-47e8-a794-486381fd1466",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d1254c8-55cc-4751-a39c-78665632c121",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}