{
    "id": "79f00120-c65d-4d96-ad53-6ca64bf6be28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_deco_fat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7872307-f248-4d89-8397-a16dda76c0fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79f00120-c65d-4d96-ad53-6ca64bf6be28",
            "compositeImage": {
                "id": "3d64dce7-039a-4a92-b91f-464b1b85a2a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7872307-f248-4d89-8397-a16dda76c0fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec214ab-6c06-48d9-a03c-5f5fca95a7ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7872307-f248-4d89-8397-a16dda76c0fe",
                    "LayerId": "24824a01-ba8f-4105-b4cd-91e75900abc0"
                }
            ]
        },
        {
            "id": "ede82b8d-ecd7-48a5-ac52-41fec9ac3f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79f00120-c65d-4d96-ad53-6ca64bf6be28",
            "compositeImage": {
                "id": "d223c962-7ddb-48fd-b0d1-19ea03afcedf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede82b8d-ecd7-48a5-ac52-41fec9ac3f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b9d1c5e-9f00-47e6-baad-5b29f8201626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede82b8d-ecd7-48a5-ac52-41fec9ac3f61",
                    "LayerId": "24824a01-ba8f-4105-b4cd-91e75900abc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "24824a01-ba8f-4105-b4cd-91e75900abc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79f00120-c65d-4d96-ad53-6ca64bf6be28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}