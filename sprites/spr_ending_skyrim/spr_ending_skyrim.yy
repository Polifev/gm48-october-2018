{
    "id": "0559ef10-1fdc-4c26-8785-dbfa68ed8a3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_skyrim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cb697e8-e3b0-41d3-b79d-dd89732d3960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0559ef10-1fdc-4c26-8785-dbfa68ed8a3c",
            "compositeImage": {
                "id": "cffd8130-4f4d-4843-b1a7-3cefe1e3c89b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb697e8-e3b0-41d3-b79d-dd89732d3960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b31025a1-12a5-4aff-a2d4-c9d3d043f839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb697e8-e3b0-41d3-b79d-dd89732d3960",
                    "LayerId": "3b31f666-95f6-412d-99ed-44b44109a2a3"
                }
            ]
        },
        {
            "id": "6f9ac330-6600-4d3d-ae0a-b2e2fc5db6b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0559ef10-1fdc-4c26-8785-dbfa68ed8a3c",
            "compositeImage": {
                "id": "8083eef4-4e7b-4e9c-a7ad-5fec1950c6f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f9ac330-6600-4d3d-ae0a-b2e2fc5db6b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a944ef-7fa9-49dd-a0a9-73c16f2fd9ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f9ac330-6600-4d3d-ae0a-b2e2fc5db6b3",
                    "LayerId": "3b31f666-95f6-412d-99ed-44b44109a2a3"
                }
            ]
        },
        {
            "id": "0131fdc1-2a6e-44d9-975e-05cdbe504cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0559ef10-1fdc-4c26-8785-dbfa68ed8a3c",
            "compositeImage": {
                "id": "3489b8be-7681-4009-ab81-40dbe67d3594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0131fdc1-2a6e-44d9-975e-05cdbe504cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0454e156-0746-4d55-a3ee-f42d71f03dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0131fdc1-2a6e-44d9-975e-05cdbe504cef",
                    "LayerId": "3b31f666-95f6-412d-99ed-44b44109a2a3"
                }
            ]
        },
        {
            "id": "8460d4bf-9430-488e-9edf-1728305e1b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0559ef10-1fdc-4c26-8785-dbfa68ed8a3c",
            "compositeImage": {
                "id": "459c16ff-7564-4bf7-8539-d23d8e0938ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8460d4bf-9430-488e-9edf-1728305e1b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee32da82-4bad-48c4-86ef-0099670eb583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8460d4bf-9430-488e-9edf-1728305e1b72",
                    "LayerId": "3b31f666-95f6-412d-99ed-44b44109a2a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "3b31f666-95f6-412d-99ed-44b44109a2a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0559ef10-1fdc-4c26-8785-dbfa68ed8a3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}