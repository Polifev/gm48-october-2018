{
    "id": "da51f8b2-1bce-4923-bece-71914e1656bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enjoy_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54d7a762-05b7-4f95-8de7-5e5f10c966f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da51f8b2-1bce-4923-bece-71914e1656bf",
            "compositeImage": {
                "id": "e103933e-b97d-4687-a548-012aa548909d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d7a762-05b7-4f95-8de7-5e5f10c966f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc48610-a01a-45a0-a0d4-01f0bb6aa304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d7a762-05b7-4f95-8de7-5e5f10c966f3",
                    "LayerId": "e26a1dbc-4ece-47db-8494-036f853ada1a"
                }
            ]
        },
        {
            "id": "d890b5db-a598-4528-b749-524add26766e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da51f8b2-1bce-4923-bece-71914e1656bf",
            "compositeImage": {
                "id": "2c34b897-a91b-4fe4-8ddf-bc80a2bb6e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d890b5db-a598-4528-b749-524add26766e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df02afe8-4f23-43ef-90de-241970048076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d890b5db-a598-4528-b749-524add26766e",
                    "LayerId": "e26a1dbc-4ece-47db-8494-036f853ada1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e26a1dbc-4ece-47db-8494-036f853ada1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da51f8b2-1bce-4923-bece-71914e1656bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}