{
    "id": "7e8d59b2-9900-42f4-bfc8-232ee329e519",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_gremlin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c222b1b-5976-4df3-b108-0c539f201f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e8d59b2-9900-42f4-bfc8-232ee329e519",
            "compositeImage": {
                "id": "5baebeaa-2a6c-4f6e-8905-86c5a5df90cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c222b1b-5976-4df3-b108-0c539f201f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c634760-c220-4214-828e-cf39b05574ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c222b1b-5976-4df3-b108-0c539f201f56",
                    "LayerId": "81167548-429c-4710-b503-a09db00d6b8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "81167548-429c-4710-b503-a09db00d6b8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e8d59b2-9900-42f4-bfc8-232ee329e519",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}