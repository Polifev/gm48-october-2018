{
    "id": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_bug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8644832-86e2-4659-b623-d6b08a80969c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "cbeab957-1848-471e-b499-00c29be133f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8644832-86e2-4659-b623-d6b08a80969c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e05fd705-b8ac-4388-9fec-d78f11176c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8644832-86e2-4659-b623-d6b08a80969c",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "1e0c4f18-9602-48cd-b9bf-ec4f7b58c9a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "0e1134b1-d85c-4d95-aa9d-cb6bd96cc495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0c4f18-9602-48cd-b9bf-ec4f7b58c9a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "423d25d9-52e4-428f-9a5b-6ffa6f93e6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0c4f18-9602-48cd-b9bf-ec4f7b58c9a9",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "16fd0ab6-c739-4705-8d1a-04b870b1aaf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "a7a4ae81-7717-458f-9229-45dcca30e703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16fd0ab6-c739-4705-8d1a-04b870b1aaf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f223bf2-33c9-4a0b-bdb9-29ba13001646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16fd0ab6-c739-4705-8d1a-04b870b1aaf3",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "d0992210-9ec7-45e1-a84b-ed7a7cbfea38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "0daf0728-c1a5-4118-8255-fe24073ad77a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0992210-9ec7-45e1-a84b-ed7a7cbfea38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "839d35f9-6295-459b-b9f4-b0556eaeb9b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0992210-9ec7-45e1-a84b-ed7a7cbfea38",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "da4a5431-c331-41c5-b7cf-632dd0e6be90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "54796815-1ef2-42a3-aebe-817997fb7f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4a5431-c331-41c5-b7cf-632dd0e6be90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef57cd6e-7713-4620-8024-c2c2858f7dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4a5431-c331-41c5-b7cf-632dd0e6be90",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "b83ea50b-0677-4203-943c-e0c63597e12b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "a658ad51-2e83-4213-b794-aac58ad9d523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83ea50b-0677-4203-943c-e0c63597e12b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9599bb0d-ae9c-46a5-b96a-1401bb4261e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83ea50b-0677-4203-943c-e0c63597e12b",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "d3a19605-17cb-4809-9af3-46e954d5c3c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "b8bc70a3-7471-4967-8d6c-efe41c23390e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a19605-17cb-4809-9af3-46e954d5c3c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bbf0c33-e2c0-4a3b-9c2a-6b684d5ca41d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a19605-17cb-4809-9af3-46e954d5c3c4",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "75417487-fde5-49c3-898a-e92ef3086156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "0bfcc14f-d69f-4bdb-8b50-a22feb56365d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75417487-fde5-49c3-898a-e92ef3086156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1df278-0ac2-42e6-b93c-93c9fe5004ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75417487-fde5-49c3-898a-e92ef3086156",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "a9eab93b-09bd-4a39-86e4-22937ebf408b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "810565f9-412f-40ec-84db-62959d5c4327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9eab93b-09bd-4a39-86e4-22937ebf408b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db63b4aa-4edf-4787-8134-ec5ce58c00d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9eab93b-09bd-4a39-86e4-22937ebf408b",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "1d45c823-1a98-4a9e-b9b4-7900cff97d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "eda69595-8956-45d7-bb47-ece888026bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d45c823-1a98-4a9e-b9b4-7900cff97d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b22b77-6eb8-4375-9db7-c33756733a7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d45c823-1a98-4a9e-b9b4-7900cff97d7a",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "0e994719-3248-4727-a04a-c7e3a3da6701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "4c759a2e-02d6-44a0-9f7b-ae6c194acec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e994719-3248-4727-a04a-c7e3a3da6701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8c7cfdc-c44d-47b7-a6ba-1dec1a09b7e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e994719-3248-4727-a04a-c7e3a3da6701",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "83881968-1555-41c6-8aa4-7d175917bd17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "73953505-d9d1-4da6-8859-e7b6628050ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83881968-1555-41c6-8aa4-7d175917bd17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "314617d2-ae0a-418b-89c8-711883d0bb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83881968-1555-41c6-8aa4-7d175917bd17",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "7dc150d4-7621-40b3-b404-3ef2c0263b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "50f1bf76-26f9-4933-8056-d80826c7538e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc150d4-7621-40b3-b404-3ef2c0263b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dbc72c9-25f9-41ce-b1df-72af150004c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc150d4-7621-40b3-b404-3ef2c0263b42",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "83747442-38f9-48b5-a82c-64f4c7a11dd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "88fe3c11-c573-45fe-ad92-a76cac755966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83747442-38f9-48b5-a82c-64f4c7a11dd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843982ce-c4c1-4415-b62f-a19781810afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83747442-38f9-48b5-a82c-64f4c7a11dd9",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "8f1a8cae-235c-4f2a-be7e-6a09485be7df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "19edc489-bce5-4552-91ae-8a17f47f20af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1a8cae-235c-4f2a-be7e-6a09485be7df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50a8744a-6b51-45e2-afea-fd83f1278c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1a8cae-235c-4f2a-be7e-6a09485be7df",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        },
        {
            "id": "85c53a75-72ae-4483-a4ad-1aec01a4b614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "compositeImage": {
                "id": "54ff9c92-6f5f-41ad-a994-bd88423eda82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85c53a75-72ae-4483-a4ad-1aec01a4b614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57096191-04cc-444e-b776-dce101792698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85c53a75-72ae-4483-a4ad-1aec01a4b614",
                    "LayerId": "2d708403-f246-4331-80cc-8983d462c694"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "2d708403-f246-4331-80cc-8983d462c694",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce96754f-cef2-44f0-b7b4-8fff8f4d7f3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}