{
    "id": "c3431ced-d6ab-4b18-b2ad-5ae41a8dad02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_sad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eca6e087-e6c9-42a1-9476-4b1a0dee1528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3431ced-d6ab-4b18-b2ad-5ae41a8dad02",
            "compositeImage": {
                "id": "506b74c0-c405-47bd-b34d-32fa13e34cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca6e087-e6c9-42a1-9476-4b1a0dee1528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d0d262-09d9-478a-9c59-a0b93ec0e60e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca6e087-e6c9-42a1-9476-4b1a0dee1528",
                    "LayerId": "10639e18-67d3-4463-b261-e76bc96100f0"
                }
            ]
        },
        {
            "id": "957012c2-3745-4e99-a38c-054d17cb3cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3431ced-d6ab-4b18-b2ad-5ae41a8dad02",
            "compositeImage": {
                "id": "11f7a77f-4f43-4270-93ae-71c83178c3d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "957012c2-3745-4e99-a38c-054d17cb3cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c88baf34-86b2-48d8-b427-20544845ad46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "957012c2-3745-4e99-a38c-054d17cb3cf2",
                    "LayerId": "10639e18-67d3-4463-b261-e76bc96100f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "10639e18-67d3-4463-b261-e76bc96100f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3431ced-d6ab-4b18-b2ad-5ae41a8dad02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}