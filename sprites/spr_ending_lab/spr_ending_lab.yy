{
    "id": "3c88285d-888b-4a47-8b73-dfd255bb353a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_lab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a2fb2cf-3a49-4a20-84d2-f6953c3534c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c88285d-888b-4a47-8b73-dfd255bb353a",
            "compositeImage": {
                "id": "9ac66df6-de82-4703-b6df-b0b3194c4731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2fb2cf-3a49-4a20-84d2-f6953c3534c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b76ea4d0-b303-4bb4-9295-dc727402ed4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2fb2cf-3a49-4a20-84d2-f6953c3534c0",
                    "LayerId": "63d1a252-0b91-4d6b-afd4-d5d05b151595"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "63d1a252-0b91-4d6b-afd4-d5d05b151595",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c88285d-888b-4a47-8b73-dfd255bb353a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}