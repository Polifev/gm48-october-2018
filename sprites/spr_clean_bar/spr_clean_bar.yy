{
    "id": "b86e187c-7020-4d66-9975-0d8028222bea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clean_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "233638b3-ec94-47dd-a480-19481f7d855e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b86e187c-7020-4d66-9975-0d8028222bea",
            "compositeImage": {
                "id": "8e112ef9-0ec0-4b5b-a770-01eefca8d4f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233638b3-ec94-47dd-a480-19481f7d855e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c927ec38-e65b-4d39-97eb-dc7b7e309bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233638b3-ec94-47dd-a480-19481f7d855e",
                    "LayerId": "1e5a4492-821d-47b9-a292-f43530af6b02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e5a4492-821d-47b9-a292-f43530af6b02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b86e187c-7020-4d66-9975-0d8028222bea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}