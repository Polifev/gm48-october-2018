{
    "id": "63a80963-b503-4c46-b8c9-6092cd927037",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_drink_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e263065-27d0-4561-afc3-fe0d7880a139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63a80963-b503-4c46-b8c9-6092cd927037",
            "compositeImage": {
                "id": "f2a22b09-50e8-448c-8e87-fb972de364d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e263065-27d0-4561-afc3-fe0d7880a139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9b115e9-103c-48ae-86b3-cd7c062573a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e263065-27d0-4561-afc3-fe0d7880a139",
                    "LayerId": "b3daa5da-7c85-46e0-8417-da1f84836d25"
                }
            ]
        },
        {
            "id": "74c77a60-4a76-4cf7-9090-3cc0ebf00ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63a80963-b503-4c46-b8c9-6092cd927037",
            "compositeImage": {
                "id": "1d4a40c2-746d-4364-bddf-630ab426567d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c77a60-4a76-4cf7-9090-3cc0ebf00ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc9ec05-461f-4749-8852-be1ddc4dd5c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c77a60-4a76-4cf7-9090-3cc0ebf00ec2",
                    "LayerId": "b3daa5da-7c85-46e0-8417-da1f84836d25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b3daa5da-7c85-46e0-8417-da1f84836d25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63a80963-b503-4c46-b8c9-6092cd927037",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}