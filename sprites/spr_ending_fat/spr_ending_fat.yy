{
    "id": "fd4fc110-5db9-4fad-a579-c8cc0ba89a34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_fat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be8984b7-cb80-4f11-9ecc-c71bf34b649b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd4fc110-5db9-4fad-a579-c8cc0ba89a34",
            "compositeImage": {
                "id": "42bae821-c4cf-4ad0-a82a-bef15dcdd0ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8984b7-cb80-4f11-9ecc-c71bf34b649b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b88ffb-9179-4671-8c68-d59dd4ff0966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8984b7-cb80-4f11-9ecc-c71bf34b649b",
                    "LayerId": "88cf98ff-b8b7-49c2-8ff0-e75e1cb14560"
                }
            ]
        },
        {
            "id": "e70266cb-0897-45e9-a044-707135dd876c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd4fc110-5db9-4fad-a579-c8cc0ba89a34",
            "compositeImage": {
                "id": "a47d4a94-0ca5-417f-af64-6977b71d7499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e70266cb-0897-45e9-a044-707135dd876c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0618d5f7-72ad-4015-9ba2-08370f5c16ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e70266cb-0897-45e9-a044-707135dd876c",
                    "LayerId": "88cf98ff-b8b7-49c2-8ff0-e75e1cb14560"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "88cf98ff-b8b7-49c2-8ff0-e75e1cb14560",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd4fc110-5db9-4fad-a579-c8cc0ba89a34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}