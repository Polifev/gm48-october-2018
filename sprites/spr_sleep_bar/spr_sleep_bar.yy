{
    "id": "05f8a2d2-c507-4bef-8c9e-48d51a0e968b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sleep_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ae112d0-188c-4bd9-9813-875584309e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f8a2d2-c507-4bef-8c9e-48d51a0e968b",
            "compositeImage": {
                "id": "4fdc5f96-a0de-4cc7-b553-2f7e31ac4091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae112d0-188c-4bd9-9813-875584309e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a254742e-82a3-4ac8-be18-5416da970704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae112d0-188c-4bd9-9813-875584309e68",
                    "LayerId": "50a4e117-db2f-4e18-8331-23c58dd2841a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "50a4e117-db2f-4e18-8331-23c58dd2841a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05f8a2d2-c507-4bef-8c9e-48d51a0e968b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}