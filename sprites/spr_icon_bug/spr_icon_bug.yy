{
    "id": "a41e2d1a-812d-4ce7-9530-d9bfd5b8a1ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_bug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de333aba-a319-4b26-bfa0-e5ba03bbaa53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a41e2d1a-812d-4ce7-9530-d9bfd5b8a1ff",
            "compositeImage": {
                "id": "bcaee65e-3d7f-4f6a-addb-c9f27b140a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de333aba-a319-4b26-bfa0-e5ba03bbaa53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c5c88e-a6d8-47b0-8f16-8c1ce55167af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de333aba-a319-4b26-bfa0-e5ba03bbaa53",
                    "LayerId": "e6b8c3a7-8538-436f-aafa-7a9870c05afa"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "e6b8c3a7-8538-436f-aafa-7a9870c05afa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a41e2d1a-812d-4ce7-9530-d9bfd5b8a1ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}