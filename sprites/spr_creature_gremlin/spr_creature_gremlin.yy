{
    "id": "09f76e30-f438-490e-9031-19671a575f72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_gremlin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e953047-af3c-46a9-8451-4aef9b5c0506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09f76e30-f438-490e-9031-19671a575f72",
            "compositeImage": {
                "id": "0f4f54c0-8107-4e5a-a7cf-33ceee2441b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e953047-af3c-46a9-8451-4aef9b5c0506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd7302a-a30c-446d-b752-e9487fdb928b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e953047-af3c-46a9-8451-4aef9b5c0506",
                    "LayerId": "77cab16b-ea66-4d0f-be80-8da960d2615c"
                }
            ]
        },
        {
            "id": "86450fd0-b9d1-43a0-8f05-cb63fcdceded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09f76e30-f438-490e-9031-19671a575f72",
            "compositeImage": {
                "id": "b9a2d716-747c-45ef-b678-1747c74ff770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86450fd0-b9d1-43a0-8f05-cb63fcdceded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e95a53-2d8d-4bb5-8467-d888d2b971fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86450fd0-b9d1-43a0-8f05-cb63fcdceded",
                    "LayerId": "77cab16b-ea66-4d0f-be80-8da960d2615c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "77cab16b-ea66-4d0f-be80-8da960d2615c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09f76e30-f438-490e-9031-19671a575f72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}