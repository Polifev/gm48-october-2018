{
    "id": "ca4373c5-d725-49ee-b009-41ed5354fcef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trigger_fiole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f16d34de-a90e-4bf6-bf0f-ff1b610343c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca4373c5-d725-49ee-b009-41ed5354fcef",
            "compositeImage": {
                "id": "35e84644-0901-450a-a26e-a46c66cb6e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16d34de-a90e-4bf6-bf0f-ff1b610343c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "488e9dc4-55e4-4da2-9d6a-8fa9c545590a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16d34de-a90e-4bf6-bf0f-ff1b610343c8",
                    "LayerId": "45a9c8bf-58af-42d7-9040-540b36251a1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "45a9c8bf-58af-42d7-9040-540b36251a1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca4373c5-d725-49ee-b009-41ed5354fcef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}