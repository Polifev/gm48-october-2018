{
    "id": "1d9e793a-b6b7-4c9f-b731-6f475d3638a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_cleaning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fc6e9d0-35ce-4bd9-9d4a-99da12464c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d9e793a-b6b7-4c9f-b731-6f475d3638a7",
            "compositeImage": {
                "id": "0a2e31ea-8ccd-46ba-8b29-3f15b12ce203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc6e9d0-35ce-4bd9-9d4a-99da12464c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a05a9923-995a-4dac-92d1-99976479feae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc6e9d0-35ce-4bd9-9d4a-99da12464c89",
                    "LayerId": "3ef3a330-4513-48c9-8f1b-c79bc8a5e81f"
                }
            ]
        },
        {
            "id": "d08d799e-fe32-4bf0-8d54-ddd464530975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d9e793a-b6b7-4c9f-b731-6f475d3638a7",
            "compositeImage": {
                "id": "9325cec9-5fa5-4d8f-b161-c42ca7573a25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08d799e-fe32-4bf0-8d54-ddd464530975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b804adad-bc41-4032-aec5-744144d4175c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08d799e-fe32-4bf0-8d54-ddd464530975",
                    "LayerId": "3ef3a330-4513-48c9-8f1b-c79bc8a5e81f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3ef3a330-4513-48c9-8f1b-c79bc8a5e81f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d9e793a-b6b7-4c9f-b731-6f475d3638a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}