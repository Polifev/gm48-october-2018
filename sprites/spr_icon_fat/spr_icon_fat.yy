{
    "id": "f587f76f-d730-44f0-9723-3e3cce573270",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_fat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dd23f2b-fdd7-495a-9201-e6763d1cd4c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f587f76f-d730-44f0-9723-3e3cce573270",
            "compositeImage": {
                "id": "eaa02e63-da50-4599-b4bb-f07c6c1d4d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd23f2b-fdd7-495a-9201-e6763d1cd4c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc14837-4784-45cd-bfca-00556519995b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd23f2b-fdd7-495a-9201-e6763d1cd4c8",
                    "LayerId": "8dde28b1-ea19-4b0b-9c39-08a8cc7fd39a"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "8dde28b1-ea19-4b0b-9c39-08a8cc7fd39a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f587f76f-d730-44f0-9723-3e3cce573270",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}