{
    "id": "8eddaa54-0038-4637-81fb-f2bcb3542442",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_enjoying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99dfb455-517c-4926-97d6-99c4c2f9a26f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eddaa54-0038-4637-81fb-f2bcb3542442",
            "compositeImage": {
                "id": "2836cadb-328c-4d56-bc74-4a662e2c18b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99dfb455-517c-4926-97d6-99c4c2f9a26f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106cea84-65f1-4a89-952a-0a81db368e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99dfb455-517c-4926-97d6-99c4c2f9a26f",
                    "LayerId": "6c4a1831-5df0-4eb0-8e4a-a0961c060eea"
                }
            ]
        },
        {
            "id": "9bb163fa-663a-4344-9550-e5f5bb54ddfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eddaa54-0038-4637-81fb-f2bcb3542442",
            "compositeImage": {
                "id": "3d5df8e3-02c7-4945-8b9f-d1e194fc7328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb163fa-663a-4344-9550-e5f5bb54ddfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a3e2a66-7e80-4eb2-99a3-cca3b98f40a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb163fa-663a-4344-9550-e5f5bb54ddfc",
                    "LayerId": "6c4a1831-5df0-4eb0-8e4a-a0961c060eea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6c4a1831-5df0-4eb0-8e4a-a0961c060eea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8eddaa54-0038-4637-81fb-f2bcb3542442",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}