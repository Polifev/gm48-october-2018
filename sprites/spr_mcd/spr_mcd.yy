{
    "id": "5de9edda-8d8e-4535-8b06-2c10b213e03e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mcd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c34a8ca-9db5-4292-8a89-8d7644249c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5de9edda-8d8e-4535-8b06-2c10b213e03e",
            "compositeImage": {
                "id": "05d1daca-8e29-42ce-b39b-aebeaed46a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c34a8ca-9db5-4292-8a89-8d7644249c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d169ea8-1615-46a5-ac0e-93e1dc36af9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c34a8ca-9db5-4292-8a89-8d7644249c6a",
                    "LayerId": "2b029aa6-b1bd-4a03-8a83-9ba7bb52628c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "2b029aa6-b1bd-4a03-8a83-9ba7bb52628c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5de9edda-8d8e-4535-8b06-2c10b213e03e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 0,
    "yorig": 0
}