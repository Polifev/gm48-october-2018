{
    "id": "82e8da6f-137b-486c-a559-3a88d936413e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_time",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f297b345-295b-401f-a421-f9e3f52424b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "ac226cff-e335-406f-81a8-1af7944d5356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f297b345-295b-401f-a421-f9e3f52424b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9c49156-caab-4b96-92ab-2da71e8141b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f297b345-295b-401f-a421-f9e3f52424b4",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "63f45307-db4e-4094-8c9d-5c46e569ae19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "0f4ac28c-ea24-417d-9c50-ced27830a01e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63f45307-db4e-4094-8c9d-5c46e569ae19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3540bf77-9fbc-433d-8efc-26fe7029613b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63f45307-db4e-4094-8c9d-5c46e569ae19",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "bbc9a279-134f-4d21-ae04-24a755d6627e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "d1f8da6b-03b9-49a0-994b-77a5fdb5e116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc9a279-134f-4d21-ae04-24a755d6627e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ee80cf-723b-4cd0-8605-a8a02b9a3105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc9a279-134f-4d21-ae04-24a755d6627e",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "9a92b40a-05bd-4092-b211-79bbb37ee5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "e71c522a-4e96-4b02-b0bb-62aa759464e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a92b40a-05bd-4092-b211-79bbb37ee5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b91f886-048a-4f34-ba0a-222907c5f42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a92b40a-05bd-4092-b211-79bbb37ee5f2",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "7d0c8477-723f-4f53-85ca-7c873418cbfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "bcfef793-ac53-45c7-84bb-de34ee5018f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0c8477-723f-4f53-85ca-7c873418cbfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eebac0ed-be94-46c5-87ec-e26104e6d10d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0c8477-723f-4f53-85ca-7c873418cbfb",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "08b8b74b-6379-48e3-b1ec-decba460aff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "078841f0-fc47-437e-9bc9-bc5fc9db8fee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08b8b74b-6379-48e3-b1ec-decba460aff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59aca4a-d26e-44ef-8344-340642cbdf24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08b8b74b-6379-48e3-b1ec-decba460aff2",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "d50eb2f3-2102-42f2-bb01-68da8abdd355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "aedb7aa5-e8fa-4f6c-b366-cc97b65883fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d50eb2f3-2102-42f2-bb01-68da8abdd355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a96938d-f320-4fde-9995-859f49a81c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d50eb2f3-2102-42f2-bb01-68da8abdd355",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        },
        {
            "id": "665f02bf-e667-48a9-9958-c99ec6336fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "compositeImage": {
                "id": "ed192b1b-d8bb-4dde-b798-ce442ae1aca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "665f02bf-e667-48a9-9958-c99ec6336fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ddabd34-5560-431b-92fb-80d506883a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "665f02bf-e667-48a9-9958-c99ec6336fc7",
                    "LayerId": "f13e9e23-d196-4cfb-85ea-75973070b90d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "f13e9e23-d196-4cfb-85ea-75973070b90d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82e8da6f-137b-486c-a559-3a88d936413e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}