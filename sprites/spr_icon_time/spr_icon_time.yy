{
    "id": "49746ed6-065f-489d-ac56-53df6dfda6bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_time",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 20,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "060e11ff-bbfe-4fe6-998d-ebfd24a4f9fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49746ed6-065f-489d-ac56-53df6dfda6bb",
            "compositeImage": {
                "id": "4e97e877-3aad-42ad-b7e0-94768b4fd600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "060e11ff-bbfe-4fe6-998d-ebfd24a4f9fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc6a3920-287e-48f6-88b3-544e3a538d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "060e11ff-bbfe-4fe6-998d-ebfd24a4f9fa",
                    "LayerId": "b51dd337-acba-4e26-a60f-e4a04f91ad5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b51dd337-acba-4e26-a60f-e4a04f91ad5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49746ed6-065f-489d-ac56-53df6dfda6bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}