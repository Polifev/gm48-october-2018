{
    "id": "e2fcf10c-480c-4d4c-bdda-d6308e2f5a55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_dontstarve",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1405d51d-54f4-4fe4-8c1d-d139be0ed635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2fcf10c-480c-4d4c-bdda-d6308e2f5a55",
            "compositeImage": {
                "id": "b0f6d7aa-dcc2-4909-971b-fd42c8f3a9bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1405d51d-54f4-4fe4-8c1d-d139be0ed635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeda3af4-0090-48fc-9a22-683103d89b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1405d51d-54f4-4fe4-8c1d-d139be0ed635",
                    "LayerId": "40654c07-aa19-4615-91d3-668d8957b875"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "40654c07-aa19-4615-91d3-668d8957b875",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2fcf10c-480c-4d4c-bdda-d6308e2f5a55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}