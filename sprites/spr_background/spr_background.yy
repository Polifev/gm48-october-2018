{
    "id": "5ab0b698-328a-4d12-a1bc-62ed5d265530",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d465be5-bdcf-4ad4-9d48-c144b0009a08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ab0b698-328a-4d12-a1bc-62ed5d265530",
            "compositeImage": {
                "id": "e648311c-064f-4ce3-a1f6-e0d5ff74040f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d465be5-bdcf-4ad4-9d48-c144b0009a08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e90dda68-0bb9-4c73-8e90-baa44048ff4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d465be5-bdcf-4ad4-9d48-c144b0009a08",
                    "LayerId": "ce0addd7-0d04-4413-a32f-91e850d93799"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "ce0addd7-0d04-4413-a32f-91e850d93799",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ab0b698-328a-4d12-a1bc-62ed5d265530",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}