{
    "id": "d831972a-8210-46d9-bada-c189e7ecfbea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_drink_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1030daa5-75b3-4e36-a4f0-dcf920250aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d831972a-8210-46d9-bada-c189e7ecfbea",
            "compositeImage": {
                "id": "1d96289f-8f10-449b-b529-0de6f71ba4e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1030daa5-75b3-4e36-a4f0-dcf920250aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fac29a9-d698-4578-9377-88dafcf1f9c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1030daa5-75b3-4e36-a4f0-dcf920250aa9",
                    "LayerId": "f24c7354-2d77-4840-9141-66a5a825f95d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f24c7354-2d77-4840-9141-66a5a825f95d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d831972a-8210-46d9-bada-c189e7ecfbea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}