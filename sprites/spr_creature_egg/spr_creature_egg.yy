{
    "id": "09da3f6c-b40b-4f28-9404-18e4933f80be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_egg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8db3622c-3dd3-42f7-bef6-b6526618d1c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da3f6c-b40b-4f28-9404-18e4933f80be",
            "compositeImage": {
                "id": "7c226414-fcca-4d2f-b79c-f877c3a39dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db3622c-3dd3-42f7-bef6-b6526618d1c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df10d7d0-f17c-41fa-9d44-acc9c8aa53a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db3622c-3dd3-42f7-bef6-b6526618d1c9",
                    "LayerId": "47393da2-8bbf-46fb-858c-80cff39547d3"
                }
            ]
        },
        {
            "id": "32ed9cd7-8b3a-4884-9383-68ba15b4ccb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da3f6c-b40b-4f28-9404-18e4933f80be",
            "compositeImage": {
                "id": "d4eccd07-9a31-44fc-9c07-874bbecf8964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32ed9cd7-8b3a-4884-9383-68ba15b4ccb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb3a225d-0c61-4e75-85bb-6989c6fc2812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32ed9cd7-8b3a-4884-9383-68ba15b4ccb0",
                    "LayerId": "47393da2-8bbf-46fb-858c-80cff39547d3"
                }
            ]
        },
        {
            "id": "9a9d15e9-aaac-405d-affa-17d7402a6ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da3f6c-b40b-4f28-9404-18e4933f80be",
            "compositeImage": {
                "id": "5c580396-3898-4973-8d45-4cbbc87af3e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9d15e9-aaac-405d-affa-17d7402a6ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276bc7f8-6564-4d69-8a59-0b669f902dcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9d15e9-aaac-405d-affa-17d7402a6ddf",
                    "LayerId": "47393da2-8bbf-46fb-858c-80cff39547d3"
                }
            ]
        },
        {
            "id": "d62d1d10-9b40-45a5-b0f3-9a77e5cfdbc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da3f6c-b40b-4f28-9404-18e4933f80be",
            "compositeImage": {
                "id": "41e2ad65-d546-4283-b2cb-79725195e608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62d1d10-9b40-45a5-b0f3-9a77e5cfdbc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c99cddb1-da80-4d55-a568-b99fb5960cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62d1d10-9b40-45a5-b0f3-9a77e5cfdbc0",
                    "LayerId": "47393da2-8bbf-46fb-858c-80cff39547d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "47393da2-8bbf-46fb-858c-80cff39547d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09da3f6c-b40b-4f28-9404-18e4933f80be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}