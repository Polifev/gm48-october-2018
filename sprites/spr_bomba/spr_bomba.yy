{
    "id": "9edc642f-f76e-496a-a28c-b7b84a041b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bomba",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1b6bdae-5d4b-4680-bffd-7f38df5109aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "e16462da-f5ad-41e9-ab56-ca5856c1c019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1b6bdae-5d4b-4680-bffd-7f38df5109aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1dc27c9-f9db-462a-aba7-aeba6ddd1cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1b6bdae-5d4b-4680-bffd-7f38df5109aa",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "97e43a2a-953d-44c3-aae9-976611338010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "951b45dd-2f08-429e-a878-efc6f6e84543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e43a2a-953d-44c3-aae9-976611338010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e4fad3d-20cb-417a-a860-198cef1d48cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e43a2a-953d-44c3-aae9-976611338010",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "12d5f1ca-bd7a-4a6d-ae9c-15f25f6bacdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "e60cdb3d-1550-4aae-9643-9f35d3b6c3d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d5f1ca-bd7a-4a6d-ae9c-15f25f6bacdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709c73a6-50b4-4169-91c0-28bd1e7a0c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d5f1ca-bd7a-4a6d-ae9c-15f25f6bacdc",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "e4b01ee1-0caf-4817-b8e7-cb4d0f83863d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "bcc0f28b-7fb0-46cf-a9cc-d533b16cce8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b01ee1-0caf-4817-b8e7-cb4d0f83863d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68e5731f-a3c8-4c21-9753-50b7a84d73e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b01ee1-0caf-4817-b8e7-cb4d0f83863d",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "7863224a-88ca-46f9-8b42-3c5d7e27b3f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "d47eae34-e42c-4f22-b5ba-ad6ae4e262ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7863224a-88ca-46f9-8b42-3c5d7e27b3f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5d1b28-61d3-47c0-800c-3d170e33f117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7863224a-88ca-46f9-8b42-3c5d7e27b3f8",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "ae8ab4a5-040b-4871-8552-fac6272e433e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "34889cf1-e4e5-4dd5-9207-874d06ce7a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8ab4a5-040b-4871-8552-fac6272e433e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cdaf746-a227-4441-84aa-3917b036c7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8ab4a5-040b-4871-8552-fac6272e433e",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "8891f5be-f513-42b7-a116-43fbf0f687c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "7101e236-4de8-4564-931d-7030fa56ffeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8891f5be-f513-42b7-a116-43fbf0f687c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c31bd3-b0e9-45b6-9f23-0ced8f0b4f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8891f5be-f513-42b7-a116-43fbf0f687c6",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "d96c5591-f96b-49dd-9675-31bcf86007a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "082d8f0c-027f-4282-ae6c-da13d7fc8bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96c5591-f96b-49dd-9675-31bcf86007a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de14228-52d3-41b9-8b84-b9ef2c0bfd33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96c5591-f96b-49dd-9675-31bcf86007a9",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "e1b878ba-ac34-4706-87bb-1a38fa13823d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "4effb6cb-2686-4ac9-8a02-2be193649c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b878ba-ac34-4706-87bb-1a38fa13823d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d275606-2dfb-445b-aee6-487353291379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b878ba-ac34-4706-87bb-1a38fa13823d",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "51b98b5a-9876-4b0e-add3-f287c23037bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "c22408c9-2a95-42f6-99a6-2800bf1c57b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b98b5a-9876-4b0e-add3-f287c23037bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dca7ec5-bf1b-4d94-bd82-9e60c3478b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b98b5a-9876-4b0e-add3-f287c23037bf",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "65f53b24-4bae-4a20-9e93-42337963ec9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "c1c4288c-305b-400f-a48d-6543123b9628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f53b24-4bae-4a20-9e93-42337963ec9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a530c504-44c1-44d0-ac14-e2f54afc2c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f53b24-4bae-4a20-9e93-42337963ec9d",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "86a7abdd-1bab-4068-b59c-ee569e27e7e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "9a9f75c1-3271-45d7-811b-9406b8a45571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86a7abdd-1bab-4068-b59c-ee569e27e7e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a77933f-c375-4d61-8e9d-b4557c6d6855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86a7abdd-1bab-4068-b59c-ee569e27e7e5",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "67e9f556-7ae5-4042-b90a-06de14ca67e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "1e540ac7-d94b-4166-98ed-1f2be037c1cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e9f556-7ae5-4042-b90a-06de14ca67e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f1e758-a98d-4324-8e3c-74e1788b8b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e9f556-7ae5-4042-b90a-06de14ca67e2",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "2eb63090-9c6e-45a1-841e-97b59eede47a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "1502c0cc-3467-416c-823a-3571e0f836d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eb63090-9c6e-45a1-841e-97b59eede47a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f4a6bc5-a281-4294-b597-55f9a8f502a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eb63090-9c6e-45a1-841e-97b59eede47a",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "2de31362-205e-477e-b574-e4d04b07c87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "747c575c-f0ae-4f29-8691-4bc7d8059dfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de31362-205e-477e-b574-e4d04b07c87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a360113-0a12-473c-bcac-59081130cd2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de31362-205e-477e-b574-e4d04b07c87e",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "eafc1b06-0d48-4f29-ab9d-18f2ff45811f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "14c798e2-cc15-4431-9329-44c1e260075a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eafc1b06-0d48-4f29-ab9d-18f2ff45811f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c043da-4be1-4d42-b938-a3a1d7be50e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eafc1b06-0d48-4f29-ab9d-18f2ff45811f",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "b49397bd-f6e9-4a28-812c-24bc44b9690f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "98f67845-248d-4859-80c1-323f2072a943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b49397bd-f6e9-4a28-812c-24bc44b9690f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f753213-63b2-495a-babe-a411ae09d9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b49397bd-f6e9-4a28-812c-24bc44b9690f",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "634d9b1b-cd06-4361-a6c0-a2bbbde8ef0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "f94a5100-2213-496a-a69a-c92dd39ebbf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "634d9b1b-cd06-4361-a6c0-a2bbbde8ef0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07124b91-aeb2-4a2d-9015-6d83651f2d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "634d9b1b-cd06-4361-a6c0-a2bbbde8ef0f",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "72c88e62-6983-4503-8e20-223d9b3d6e93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "98a71dc4-7e97-4cc0-a7e3-c9cd27de0924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c88e62-6983-4503-8e20-223d9b3d6e93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffdf075-9d1f-4ec4-a1ab-55ceb1ac5d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c88e62-6983-4503-8e20-223d9b3d6e93",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "39737b4d-440f-474e-b504-6dccec9e2d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "97d5e51e-41ee-4a4e-8b05-c404027bc6cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39737b4d-440f-474e-b504-6dccec9e2d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3bed2d-0cdc-4c6b-9c06-86a77e8e6a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39737b4d-440f-474e-b504-6dccec9e2d6b",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "38b1b979-202d-4bd8-8f38-a7df25404a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "63762149-2c1d-4673-9561-81ce821ec9fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b1b979-202d-4bd8-8f38-a7df25404a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb423c2-48b2-43ec-b1de-30da9d59ece9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b1b979-202d-4bd8-8f38-a7df25404a66",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "2290e9fe-c1bf-4f13-9d52-47aab1525702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "f5e2a130-c7d0-4e9d-861f-2676023cf43f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2290e9fe-c1bf-4f13-9d52-47aab1525702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d04a17c-e394-458d-a98b-9d0ada90ef12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2290e9fe-c1bf-4f13-9d52-47aab1525702",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "c26654bc-a121-4acd-b88a-9083204fd5f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "4fdfa7df-7f4e-4d1f-b6c7-d14e2183decb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26654bc-a121-4acd-b88a-9083204fd5f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7e3eba-8e61-4b3c-9825-d3ed3dc659cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26654bc-a121-4acd-b88a-9083204fd5f5",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "0dc80ae5-cb28-4970-8e31-d944d35c127d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "2c4ccd3f-58a8-4627-ab81-32f570de42e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc80ae5-cb28-4970-8e31-d944d35c127d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37a6a509-353d-41c6-8a3f-481584b93dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc80ae5-cb28-4970-8e31-d944d35c127d",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "47392df1-e02a-479d-97d6-610decbaa0ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "f298f0a9-443a-4283-b54f-35446ccba270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47392df1-e02a-479d-97d6-610decbaa0ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bda60fd-47e0-41c5-ae26-d916a9ee6507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47392df1-e02a-479d-97d6-610decbaa0ef",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "8e67087d-fa60-490b-b09a-28dee868d71c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "56b4525a-0fd6-47fa-93a3-9ab2f56984a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e67087d-fa60-490b-b09a-28dee868d71c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfafee61-151b-4276-a082-b981fc5fed4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e67087d-fa60-490b-b09a-28dee868d71c",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "88de020f-1d5d-4e79-a9b2-057ba9ef85ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "d66b05a8-34cc-470b-ba07-4bb34659f457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88de020f-1d5d-4e79-a9b2-057ba9ef85ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d2fb01-d503-456d-a2ea-fb25f5faf7ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88de020f-1d5d-4e79-a9b2-057ba9ef85ca",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "075336c0-7d07-4f4d-a5e1-6f8e1e9645c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "bc156351-4b18-4a60-9c4e-5d5080e461bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "075336c0-7d07-4f4d-a5e1-6f8e1e9645c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "415fbf76-0b76-4f57-9572-ffdd063fde47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "075336c0-7d07-4f4d-a5e1-6f8e1e9645c7",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "25dcb502-b5b1-400a-8294-92c0de12c484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "00613390-939b-43ec-8140-dd6769ea1c7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25dcb502-b5b1-400a-8294-92c0de12c484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c9759f-8f59-47c8-8ada-4e322c6c9695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25dcb502-b5b1-400a-8294-92c0de12c484",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "2fe6b7b3-c141-4d3f-9472-8e1e47fed291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "eeefab0e-4903-4025-b055-b266d236bfe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe6b7b3-c141-4d3f-9472-8e1e47fed291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d94b4a-1f0f-4c3b-95f9-edcf7fa54591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe6b7b3-c141-4d3f-9472-8e1e47fed291",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "f41eee5d-206f-4ec5-8950-86199ba49de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "c2c7b29c-62a6-49f5-967b-9a5678c6081f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f41eee5d-206f-4ec5-8950-86199ba49de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be4bc02-bc2f-40c0-be09-fa2a6d6107df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f41eee5d-206f-4ec5-8950-86199ba49de6",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "0d088d6c-c606-45a5-90be-8ce999896f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "c3d21a70-5188-4b24-889f-2608b0aafa88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d088d6c-c606-45a5-90be-8ce999896f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f6f426-e0b3-4142-9fbe-61b7c2858cdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d088d6c-c606-45a5-90be-8ce999896f5d",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "f374eab8-194e-40eb-88d0-2931a600a9fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "798908a3-d36a-4e6f-abb1-09c0e819332c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f374eab8-194e-40eb-88d0-2931a600a9fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def93950-dd01-4fe3-bd38-f6a7e02b198a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f374eab8-194e-40eb-88d0-2931a600a9fe",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "c3805e54-e998-49c2-bf78-a7b4ee08e4d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "9dfd47fe-9220-43f4-a20f-df927aef96d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3805e54-e998-49c2-bf78-a7b4ee08e4d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "410d8701-a036-474d-8281-47e8336043ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3805e54-e998-49c2-bf78-a7b4ee08e4d7",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "a4fc0fc3-b556-4026-8a53-c739ee963b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "0390ad31-7c77-4d8c-b20b-1665974a83f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4fc0fc3-b556-4026-8a53-c739ee963b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffcf5cfc-3b2e-4840-8518-6d2beb93ebba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4fc0fc3-b556-4026-8a53-c739ee963b84",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "33d06517-22d9-499e-b197-f084b5ae799c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "a54d2e5c-6b34-468c-9f29-9fd82ef66dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d06517-22d9-499e-b197-f084b5ae799c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f1a6c5-2129-4e03-a3e9-3089b2001e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d06517-22d9-499e-b197-f084b5ae799c",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "8a4c9dbe-fc11-4ccf-9df6-269ef504364f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "06a7b6dc-47c1-43fc-943a-b4ad0b2e9017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4c9dbe-fc11-4ccf-9df6-269ef504364f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccba1bec-e220-441b-a4ea-3e64c765cd2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4c9dbe-fc11-4ccf-9df6-269ef504364f",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "071cdfad-4d88-4b71-9c2e-3d52ac872719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "7ca90ab7-4b7b-4142-96d8-ba74f60fb127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "071cdfad-4d88-4b71-9c2e-3d52ac872719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "690810fd-9436-44f4-9be7-962bba9167ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "071cdfad-4d88-4b71-9c2e-3d52ac872719",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "c3c8f4af-965d-458b-9141-61b8d190ecff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "e2dddef9-8fbc-4e8d-8fd3-10aeec8da267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3c8f4af-965d-458b-9141-61b8d190ecff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2045ab3-907f-4de1-a151-8f7dfafadf3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3c8f4af-965d-458b-9141-61b8d190ecff",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "d6d474a1-e5d8-4764-a105-b54543cf73e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "831aa5e2-8bd3-4540-93a5-e2718d5f75e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d474a1-e5d8-4764-a105-b54543cf73e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eb558d9-377f-46a7-9a29-45e2bcd71873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d474a1-e5d8-4764-a105-b54543cf73e1",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "c45a1058-5ecd-42c3-bb44-aea9af4c429e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "7aea46f8-4b4a-4da7-b629-b6d7f22fadb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45a1058-5ecd-42c3-bb44-aea9af4c429e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e369482-c13d-4bc1-8db7-e5b1387a299f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45a1058-5ecd-42c3-bb44-aea9af4c429e",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "827d3cdf-a5fa-4ab2-9c02-f99b7814b735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "27750af1-fce5-4f7e-b734-8dedc2dc976e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827d3cdf-a5fa-4ab2-9c02-f99b7814b735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cb92208-575d-4a8b-969a-9ca386bbb78b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827d3cdf-a5fa-4ab2-9c02-f99b7814b735",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "1f85b8d0-8432-4636-9cd7-4cb765b4d201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "7bf1bc85-049e-431f-863f-59668ee6767c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f85b8d0-8432-4636-9cd7-4cb765b4d201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bfd7454-8171-4780-a785-c7e4d350ab93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f85b8d0-8432-4636-9cd7-4cb765b4d201",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "b02202a8-568c-4c84-a1c8-aeb73c339122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "4ac24b8b-7684-4d6b-870e-941fac85d0f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b02202a8-568c-4c84-a1c8-aeb73c339122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e36e3a-46e7-4921-80c1-428dd7f3bed9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b02202a8-568c-4c84-a1c8-aeb73c339122",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "ca35de9c-8e7a-4555-ba32-db92149a01bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "14ea46f9-4cd5-4f56-b3ab-78318e73db93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca35de9c-8e7a-4555-ba32-db92149a01bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f9e4fbd-3a66-4ada-b143-449dd8b421de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca35de9c-8e7a-4555-ba32-db92149a01bc",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "a47137ee-65c9-4648-b65a-8fec4fe24fda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "b31649ac-6996-4c8a-9276-9b005a4efd75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a47137ee-65c9-4648-b65a-8fec4fe24fda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46d6fb3-f309-4e9e-9952-4942bdeb7473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a47137ee-65c9-4648-b65a-8fec4fe24fda",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "98fca1be-3673-43fe-a10a-54e16c1bb153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "74dfe0f1-b858-4a28-a8f7-ca9c7ccfbf8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fca1be-3673-43fe-a10a-54e16c1bb153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecdcce5-37a5-4166-a116-a034d4ed9385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fca1be-3673-43fe-a10a-54e16c1bb153",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "44595281-fb34-4332-b18e-297fb88271a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "2eacefae-c667-48e9-9ddf-cbce31c3312a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44595281-fb34-4332-b18e-297fb88271a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1830a0-93fd-4047-9daf-abd0f9805e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44595281-fb34-4332-b18e-297fb88271a5",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "7fe6c9d5-98ca-496b-9fc7-9f8891ada50a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "18bbd9ed-7fdd-4f6d-ba32-578af7f75a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe6c9d5-98ca-496b-9fc7-9f8891ada50a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46fd3a2a-3d1e-4b35-9b4d-fbbd65e2634d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe6c9d5-98ca-496b-9fc7-9f8891ada50a",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        },
        {
            "id": "dc2a10bb-993a-40d2-9605-52746eb9e9b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "compositeImage": {
                "id": "21246d46-f602-48f6-b549-8db3d5b1ad59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc2a10bb-993a-40d2-9605-52746eb9e9b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a18dba4-b1f5-4f53-8e24-880e0d733644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2a10bb-993a-40d2-9605-52746eb9e9b3",
                    "LayerId": "03fe8db8-2187-4892-a4db-a2dc53d406c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "03fe8db8-2187-4892-a4db-a2dc53d406c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9edc642f-f76e-496a-a28c-b7b84a041b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}