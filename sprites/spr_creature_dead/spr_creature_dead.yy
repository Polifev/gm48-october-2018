{
    "id": "6b0a4424-bfa5-41dd-ac9c-e04cf58ac7d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2463626-0265-4636-a789-ec05dc33f1cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b0a4424-bfa5-41dd-ac9c-e04cf58ac7d9",
            "compositeImage": {
                "id": "d48c397b-bc78-4a9c-9b9c-e832db07db45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2463626-0265-4636-a789-ec05dc33f1cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf0b61c-6ae5-48e8-9a67-27f9eb710fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2463626-0265-4636-a789-ec05dc33f1cc",
                    "LayerId": "7e98eb3b-eca7-4dbf-a0f0-28c1b4870dde"
                }
            ]
        },
        {
            "id": "9707db95-c717-4932-be38-7a207acb6421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b0a4424-bfa5-41dd-ac9c-e04cf58ac7d9",
            "compositeImage": {
                "id": "bf3ead0a-9b6c-420c-84cc-796cf55c51ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9707db95-c717-4932-be38-7a207acb6421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e511d721-4757-4263-a6be-6eadc20df81d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9707db95-c717-4932-be38-7a207acb6421",
                    "LayerId": "7e98eb3b-eca7-4dbf-a0f0-28c1b4870dde"
                }
            ]
        },
        {
            "id": "168f9036-a219-44f7-b7de-3f141844242a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b0a4424-bfa5-41dd-ac9c-e04cf58ac7d9",
            "compositeImage": {
                "id": "5c046523-61d8-4f41-857a-01839c8e56e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "168f9036-a219-44f7-b7de-3f141844242a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09dac192-2a62-404c-a93d-2b88d2b69acc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "168f9036-a219-44f7-b7de-3f141844242a",
                    "LayerId": "7e98eb3b-eca7-4dbf-a0f0-28c1b4870dde"
                }
            ]
        },
        {
            "id": "830b0c5d-0794-44b3-8037-c4e08513fc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b0a4424-bfa5-41dd-ac9c-e04cf58ac7d9",
            "compositeImage": {
                "id": "652bf63a-91d8-4d85-b370-6f7adfe8a666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830b0c5d-0794-44b3-8037-c4e08513fc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42065de-f72d-4f29-bfd0-f7abf43db1cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830b0c5d-0794-44b3-8037-c4e08513fc07",
                    "LayerId": "7e98eb3b-eca7-4dbf-a0f0-28c1b4870dde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7e98eb3b-eca7-4dbf-a0f0-28c1b4870dde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b0a4424-bfa5-41dd-ac9c-e04cf58ac7d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}