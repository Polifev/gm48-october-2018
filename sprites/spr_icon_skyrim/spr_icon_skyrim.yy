{
    "id": "72dc1232-8ecc-44a7-8f71-d0892dc8a225",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_skyrim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 5,
    "bbox_right": 30,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "080ed705-f3d7-4b94-875e-2d7302af71d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dc1232-8ecc-44a7-8f71-d0892dc8a225",
            "compositeImage": {
                "id": "da35aeed-701b-4546-899f-76f9868feb11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080ed705-f3d7-4b94-875e-2d7302af71d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15dc9340-cb8e-43ac-837d-ef6868f8cf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080ed705-f3d7-4b94-875e-2d7302af71d8",
                    "LayerId": "45b9d5ad-55af-455a-b8c8-6096c6d3f259"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "45b9d5ad-55af-455a-b8c8-6096c6d3f259",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72dc1232-8ecc-44a7-8f71-d0892dc8a225",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}