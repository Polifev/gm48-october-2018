{
    "id": "df8a83e0-3fe8-4f1f-b767-d86770688e19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_scientist",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "046c9bc1-efa8-44ef-9f53-18a94e8d395d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df8a83e0-3fe8-4f1f-b767-d86770688e19",
            "compositeImage": {
                "id": "72d8666b-63a4-43d9-816e-eb4986f9bb63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046c9bc1-efa8-44ef-9f53-18a94e8d395d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7d0321-4499-4178-acf3-197d442797d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046c9bc1-efa8-44ef-9f53-18a94e8d395d",
                    "LayerId": "8271badd-cc2c-4e2f-bbf2-32a6f1b364cb"
                }
            ]
        },
        {
            "id": "b3315405-c91f-47a6-baf3-d23bf3436dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df8a83e0-3fe8-4f1f-b767-d86770688e19",
            "compositeImage": {
                "id": "2be5f60c-82be-40e1-8218-1e1db20ce311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3315405-c91f-47a6-baf3-d23bf3436dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a05d2aac-f0ce-4743-bbe1-09c7e5bcbc5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3315405-c91f-47a6-baf3-d23bf3436dc7",
                    "LayerId": "8271badd-cc2c-4e2f-bbf2-32a6f1b364cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8271badd-cc2c-4e2f-bbf2-32a6f1b364cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df8a83e0-3fe8-4f1f-b767-d86770688e19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 32
}