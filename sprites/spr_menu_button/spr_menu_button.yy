{
    "id": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0ed877a-7313-42f5-a678-4558d0d9daf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
            "compositeImage": {
                "id": "36698ba0-6b4a-4f9f-8a73-c65e9aecd39f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ed877a-7313-42f5-a678-4558d0d9daf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c919d39-8d73-4801-8101-89eea7b70a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ed877a-7313-42f5-a678-4558d0d9daf4",
                    "LayerId": "08a0f9a4-cfb6-4378-a68d-fb9388d80293"
                }
            ]
        },
        {
            "id": "4b9d022b-a336-4794-8000-2efd65188043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
            "compositeImage": {
                "id": "53043d6c-0846-4df4-b01a-41a899556784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9d022b-a336-4794-8000-2efd65188043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036d8d1d-d552-41ec-bd02-3450a88da41b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9d022b-a336-4794-8000-2efd65188043",
                    "LayerId": "08a0f9a4-cfb6-4378-a68d-fb9388d80293"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "08a0f9a4-cfb6-4378-a68d-fb9388d80293",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a230e2f5-1910-40b5-b1b3-a920472f1a3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 6
}