{
    "id": "6fe81945-4370-4fe7-be76-8ab4aaaffe60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_calendar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fb2ce9a-cf3d-4104-a5d4-6f04da5ecc33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fe81945-4370-4fe7-be76-8ab4aaaffe60",
            "compositeImage": {
                "id": "e2f25f4b-7150-4d8b-a969-f31cd155deaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb2ce9a-cf3d-4104-a5d4-6f04da5ecc33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c403770d-c9ab-41db-850e-3b90de68d54d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb2ce9a-cf3d-4104-a5d4-6f04da5ecc33",
                    "LayerId": "36775e03-e045-421c-8559-0e3ecd9e1801"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "36775e03-e045-421c-8559-0e3ecd9e1801",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fe81945-4370-4fe7-be76-8ab4aaaffe60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}