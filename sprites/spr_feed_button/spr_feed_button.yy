{
    "id": "cacd5eac-e736-41b9-bc66-491fb7c64d1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_feed_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e9afc42-3434-445f-99d9-ea4cceba9d9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cacd5eac-e736-41b9-bc66-491fb7c64d1a",
            "compositeImage": {
                "id": "1e4e093e-2387-40f7-848a-5c40c0b4f9c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e9afc42-3434-445f-99d9-ea4cceba9d9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c77856-13b1-462e-b131-251568d06d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e9afc42-3434-445f-99d9-ea4cceba9d9e",
                    "LayerId": "a87ab845-a1d1-448e-b0e6-96844d4eee0b"
                }
            ]
        },
        {
            "id": "bf53f34d-4689-418b-8888-85ba5256c3c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cacd5eac-e736-41b9-bc66-491fb7c64d1a",
            "compositeImage": {
                "id": "47e8eea4-d9b9-4145-9ada-22a0fde96dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf53f34d-4689-418b-8888-85ba5256c3c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b2b44c-d725-4297-a05a-c75f0138b4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf53f34d-4689-418b-8888-85ba5256c3c7",
                    "LayerId": "a87ab845-a1d1-448e-b0e6-96844d4eee0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a87ab845-a1d1-448e-b0e6-96844d4eee0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cacd5eac-e736-41b9-bc66-491fb7c64d1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}