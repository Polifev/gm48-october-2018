{
    "id": "baf1eafa-5feb-452d-95b9-c13321190e24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_asleep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "327cfd26-38fc-4a6b-9f78-69f27f93abb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baf1eafa-5feb-452d-95b9-c13321190e24",
            "compositeImage": {
                "id": "27b323b7-2a42-4f76-bcef-f4c00931b01d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "327cfd26-38fc-4a6b-9f78-69f27f93abb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78794288-a4d6-4e07-9515-cb84cde151f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "327cfd26-38fc-4a6b-9f78-69f27f93abb6",
                    "LayerId": "f2d620cf-7689-44db-aee2-9727de943116"
                }
            ]
        },
        {
            "id": "c8e35e29-836f-45a0-a02e-be8f33e86582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baf1eafa-5feb-452d-95b9-c13321190e24",
            "compositeImage": {
                "id": "e7bc7d0d-8792-4d27-9120-f9d50b4ff4d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e35e29-836f-45a0-a02e-be8f33e86582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1742b5c6-cb6c-44fc-a239-ccc30170f3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e35e29-836f-45a0-a02e-be8f33e86582",
                    "LayerId": "f2d620cf-7689-44db-aee2-9727de943116"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f2d620cf-7689-44db-aee2-9727de943116",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baf1eafa-5feb-452d-95b9-c13321190e24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}