{
    "id": "2fddd664-35a9-4664-b371-812fb4798659",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trophy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35aa8f37-f222-4488-9f53-94098316cb93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fddd664-35a9-4664-b371-812fb4798659",
            "compositeImage": {
                "id": "31157c0e-a850-4325-9498-89309e4e86a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35aa8f37-f222-4488-9f53-94098316cb93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "948bac11-3a3a-4837-b972-a1f3d9afa5c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35aa8f37-f222-4488-9f53-94098316cb93",
                    "LayerId": "a5bc7e1c-78c2-493c-ba77-578bb8729cd6"
                }
            ]
        },
        {
            "id": "cdaad229-6af2-4d04-a1b1-6fdf58962bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fddd664-35a9-4664-b371-812fb4798659",
            "compositeImage": {
                "id": "f8828a0c-603d-4250-9a75-e9c79a334cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdaad229-6af2-4d04-a1b1-6fdf58962bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22f3b373-71a5-4760-9c14-48cf1f011704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdaad229-6af2-4d04-a1b1-6fdf58962bc7",
                    "LayerId": "a5bc7e1c-78c2-493c-ba77-578bb8729cd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a5bc7e1c-78c2-493c-ba77-578bb8729cd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fddd664-35a9-4664-b371-812fb4798659",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}