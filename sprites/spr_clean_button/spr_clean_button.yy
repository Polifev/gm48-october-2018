{
    "id": "3841af53-f124-4735-86a2-8c2171f0a933",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clean_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6bf6042-3a88-4ee2-b525-8ef29580ae33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3841af53-f124-4735-86a2-8c2171f0a933",
            "compositeImage": {
                "id": "c55d190d-bd82-4f1c-9a8b-a8862a2e3c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6bf6042-3a88-4ee2-b525-8ef29580ae33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ada750-16b4-4fa9-9ac1-f7b1be43f578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6bf6042-3a88-4ee2-b525-8ef29580ae33",
                    "LayerId": "78249d8c-0ae7-4780-b336-2cace5abd4d3"
                }
            ]
        },
        {
            "id": "86185aae-f5ba-46c6-9a93-de2bced19575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3841af53-f124-4735-86a2-8c2171f0a933",
            "compositeImage": {
                "id": "18dff20b-c09f-4845-9c93-a21ca7800855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86185aae-f5ba-46c6-9a93-de2bced19575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed48d4e4-8ee4-4642-8244-f632c4c21c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86185aae-f5ba-46c6-9a93-de2bced19575",
                    "LayerId": "78249d8c-0ae7-4780-b336-2cace5abd4d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "78249d8c-0ae7-4780-b336-2cace5abd4d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3841af53-f124-4735-86a2-8c2171f0a933",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}