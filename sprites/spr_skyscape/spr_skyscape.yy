{
    "id": "d019a74f-36cb-4de5-b1f1-2fe29d291259",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skyscape",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ed5c441-9448-46ac-a220-18abd88efeab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d019a74f-36cb-4de5-b1f1-2fe29d291259",
            "compositeImage": {
                "id": "f23959db-eca7-4580-995a-205ca8324d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed5c441-9448-46ac-a220-18abd88efeab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acaf6d9b-546b-4f20-9f0c-4db77cc322f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed5c441-9448-46ac-a220-18abd88efeab",
                    "LayerId": "a2ddbb89-7870-4f6f-b90d-8e28905985a8"
                }
            ]
        },
        {
            "id": "e9d7f5ba-5bbb-4098-9510-cfe25007dc22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d019a74f-36cb-4de5-b1f1-2fe29d291259",
            "compositeImage": {
                "id": "5e842e9b-7438-4c91-b314-a84525912999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d7f5ba-5bbb-4098-9510-cfe25007dc22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61aa981f-96f3-4131-9ddb-b79031f24d92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d7f5ba-5bbb-4098-9510-cfe25007dc22",
                    "LayerId": "a2ddbb89-7870-4f6f-b90d-8e28905985a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2ddbb89-7870-4f6f-b90d-8e28905985a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d019a74f-36cb-4de5-b1f1-2fe29d291259",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}