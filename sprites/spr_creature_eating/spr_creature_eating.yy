{
    "id": "53178ce5-9733-4146-bff8-47794e5d568e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_eating",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9989da5-52ec-44f0-95f4-efd779077d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53178ce5-9733-4146-bff8-47794e5d568e",
            "compositeImage": {
                "id": "e0819f3e-83e6-484d-b773-92d1ca932e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9989da5-52ec-44f0-95f4-efd779077d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c23c694-5d09-4a55-be66-332fa7cd4d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9989da5-52ec-44f0-95f4-efd779077d51",
                    "LayerId": "255ffa64-57fb-4415-bf74-df4e4bb14ca7"
                }
            ]
        },
        {
            "id": "95726399-e97b-4c9e-a20e-02110e0d99bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53178ce5-9733-4146-bff8-47794e5d568e",
            "compositeImage": {
                "id": "f976b7cb-4730-41df-9cca-8f6c54df2a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95726399-e97b-4c9e-a20e-02110e0d99bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c7904e-c7a0-4c11-8398-ae0b790fc902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95726399-e97b-4c9e-a20e-02110e0d99bf",
                    "LayerId": "255ffa64-57fb-4415-bf74-df4e4bb14ca7"
                }
            ]
        },
        {
            "id": "ed8e7980-7858-44a8-9be2-2cb73780a202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53178ce5-9733-4146-bff8-47794e5d568e",
            "compositeImage": {
                "id": "6b275ce1-fd14-498a-a14b-66311c7901ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed8e7980-7858-44a8-9be2-2cb73780a202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c96f7e2-c656-4b9d-8f89-5f63c6df931e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed8e7980-7858-44a8-9be2-2cb73780a202",
                    "LayerId": "255ffa64-57fb-4415-bf74-df4e4bb14ca7"
                }
            ]
        },
        {
            "id": "755192a0-8c9b-4e47-9a3b-b7ce7750ff08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53178ce5-9733-4146-bff8-47794e5d568e",
            "compositeImage": {
                "id": "244b28bc-e233-48c9-909c-6475074183a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "755192a0-8c9b-4e47-9a3b-b7ce7750ff08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d72fb9-7065-426d-a95a-73b0d9e6c5b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "755192a0-8c9b-4e47-9a3b-b7ce7750ff08",
                    "LayerId": "255ffa64-57fb-4415-bf74-df4e4bb14ca7"
                }
            ]
        },
        {
            "id": "582aa661-69da-4256-af53-f1a4cb092de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53178ce5-9733-4146-bff8-47794e5d568e",
            "compositeImage": {
                "id": "e6d2a1c9-f59c-4c55-a377-c3d907ab028e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582aa661-69da-4256-af53-f1a4cb092de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f900a509-cc1b-4e5f-bfa9-6ad0a921a48c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582aa661-69da-4256-af53-f1a4cb092de8",
                    "LayerId": "255ffa64-57fb-4415-bf74-df4e4bb14ca7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "255ffa64-57fb-4415-bf74-df4e4bb14ca7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53178ce5-9733-4146-bff8-47794e5d568e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}