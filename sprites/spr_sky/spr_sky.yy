{
    "id": "93cb53f7-9a96-4526-aac4-6d5613a2fe41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb54aacb-29ea-4fec-acf3-0dd54be7e048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93cb53f7-9a96-4526-aac4-6d5613a2fe41",
            "compositeImage": {
                "id": "7d3bde95-add2-4d74-9c83-8d7ae8cfcda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb54aacb-29ea-4fec-acf3-0dd54be7e048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705ed6b0-f83b-4cd9-8af8-1108818a93ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb54aacb-29ea-4fec-acf3-0dd54be7e048",
                    "LayerId": "8301e114-7fbe-40ae-9a3e-7d36cc25cd03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "8301e114-7fbe-40ae-9a3e-7d36cc25cd03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93cb53f7-9a96-4526-aac4-6d5613a2fe41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}