{
    "id": "6a82378f-627a-46c7-8610-44f607e442f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 1,
    "bbox_right": 126,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "607ab985-00e5-4c10-9a07-db21dbd92f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a82378f-627a-46c7-8610-44f607e442f0",
            "compositeImage": {
                "id": "31c503dc-051f-44cc-ab87-456d9d0b8066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607ab985-00e5-4c10-9a07-db21dbd92f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f07f6fd4-7a2e-4751-9a16-a0c1216b5036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607ab985-00e5-4c10-9a07-db21dbd92f5d",
                    "LayerId": "bc0cd6f1-ebcc-4f5d-81c0-c6bb105dc7e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc0cd6f1-ebcc-4f5d-81c0-c6bb105dc7e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a82378f-627a-46c7-8610-44f607e442f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}