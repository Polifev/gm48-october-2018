{
    "id": "6d5e3f06-6b7b-4d40-9301-1dee43ad6bd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enjoy_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a4ab876-bcb8-41cf-8e3b-f3d01a089038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d5e3f06-6b7b-4d40-9301-1dee43ad6bd1",
            "compositeImage": {
                "id": "f9b2fc1e-5f6d-4e72-9deb-a8d78e894063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4ab876-bcb8-41cf-8e3b-f3d01a089038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c3ff74-d661-4434-a735-916347158ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4ab876-bcb8-41cf-8e3b-f3d01a089038",
                    "LayerId": "43e85d8c-247e-422e-b9c6-cec261d4537b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "43e85d8c-247e-422e-b9c6-cec261d4537b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d5e3f06-6b7b-4d40-9301-1dee43ad6bd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}