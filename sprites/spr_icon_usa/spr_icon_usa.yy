{
    "id": "99ba2235-320c-4e4f-86f6-301dea8aa735",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_usa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc54dd98-1a8b-4743-80a9-0af66e6f5a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99ba2235-320c-4e4f-86f6-301dea8aa735",
            "compositeImage": {
                "id": "2f53790f-dd6d-4be8-8350-23eb1f1437d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc54dd98-1a8b-4743-80a9-0af66e6f5a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22bdb6d8-5ff6-443a-b825-af5356084a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc54dd98-1a8b-4743-80a9-0af66e6f5a9f",
                    "LayerId": "332f628b-2100-4f06-b8d7-ebc592e265f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "332f628b-2100-4f06-b8d7-ebc592e265f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99ba2235-320c-4e4f-86f6-301dea8aa735",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}