{
    "id": "33228323-384f-47b9-a334-2d2eb2b8acf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ending_gremlin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c6ce103-47ed-4fe8-9b61-89faa6bfa9e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33228323-384f-47b9-a334-2d2eb2b8acf1",
            "compositeImage": {
                "id": "6086d426-5e4b-48e1-9b1f-1ae35a0bf747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c6ce103-47ed-4fe8-9b61-89faa6bfa9e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d685724f-0091-4317-9017-9eb98645c7b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c6ce103-47ed-4fe8-9b61-89faa6bfa9e3",
                    "LayerId": "0c4c6f35-23e4-4794-ba86-fdbeb9796e89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "0c4c6f35-23e4-4794-ba86-fdbeb9796e89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33228323-384f-47b9-a334-2d2eb2b8acf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}