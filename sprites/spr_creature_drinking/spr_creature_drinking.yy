{
    "id": "517bea89-e3b0-4f68-b9a8-3814d4fc07b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_drinking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a1af72d-b71a-43f1-a990-79e6ada98403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "517bea89-e3b0-4f68-b9a8-3814d4fc07b3",
            "compositeImage": {
                "id": "078b47a1-8660-4bc1-b1de-ce1064623372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1af72d-b71a-43f1-a990-79e6ada98403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8031c70-f1eb-4a87-9f9c-7e751413719e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1af72d-b71a-43f1-a990-79e6ada98403",
                    "LayerId": "9554013a-fd95-4af7-91aa-f3f9d5d14385"
                }
            ]
        },
        {
            "id": "c5b5a6ad-c8b4-4dd4-b86a-382f061c19f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "517bea89-e3b0-4f68-b9a8-3814d4fc07b3",
            "compositeImage": {
                "id": "796d141e-6319-4213-9c59-16d0714e6919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b5a6ad-c8b4-4dd4-b86a-382f061c19f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ffcf99-21d4-4bf8-a87e-94d28d04e228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b5a6ad-c8b4-4dd4-b86a-382f061c19f1",
                    "LayerId": "9554013a-fd95-4af7-91aa-f3f9d5d14385"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9554013a-fd95-4af7-91aa-f3f9d5d14385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "517bea89-e3b0-4f68-b9a8-3814d4fc07b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}