{
    "id": "77bd9465-e0c2-4ac9-84bb-3b33637747eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creature_geek",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3115fa8e-a085-4260-bf7d-35f29b1c85d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77bd9465-e0c2-4ac9-84bb-3b33637747eb",
            "compositeImage": {
                "id": "7376dcfe-a31d-48d2-8787-928b1e5db89c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3115fa8e-a085-4260-bf7d-35f29b1c85d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee053f60-2eb7-44ca-99c2-b40935628e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3115fa8e-a085-4260-bf7d-35f29b1c85d8",
                    "LayerId": "fba79306-7ff1-4aaa-9509-9152bc0ad2e5"
                }
            ]
        },
        {
            "id": "acd3695f-5f2a-4618-9640-a7d91d9a0e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77bd9465-e0c2-4ac9-84bb-3b33637747eb",
            "compositeImage": {
                "id": "23f6fcd3-5642-4452-97e1-532be80f0117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acd3695f-5f2a-4618-9640-a7d91d9a0e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612ae111-19b4-41a2-9fd7-fc7413a6497c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd3695f-5f2a-4618-9640-a7d91d9a0e30",
                    "LayerId": "fba79306-7ff1-4aaa-9509-9152bc0ad2e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fba79306-7ff1-4aaa-9509-9152bc0ad2e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77bd9465-e0c2-4ac9-84bb-3b33637747eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}