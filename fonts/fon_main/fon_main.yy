{
    "id": "207b38f7-910c-4ea3-8556-17e9655be8d0",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fon_main",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "752b7399-83ce-458f-a4f8-a7703a662480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ae957c62-e33a-48eb-bd6a-b6a238ff638c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 86,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1551ee82-3af4-4056-ba3b-d083aa781012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 32,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dd97fe6a-717e-4d94-a1db-c200336e8544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 13
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dd4042f4-3070-477c-b790-780ac5cd8114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7b871bf8-a622-46f6-a3c0-a4eba0889d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 79,
                "y": 13
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d01da55b-ff3e-4828-9433-1d3446dca92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 13
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6342e0ff-da1a-4397-834a-c7d0e107771b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 98,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2f8c0e67-8b1f-4fd8-b948-d49a24918047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a6e898b7-30d4-490c-8863-44246f396144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "583ddbed-468a-49a6-a085-3d34b877d5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 24
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f5e9b4b4-f4f9-4fcf-9034-a559d5966afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 72,
                "y": 13
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bd71cdf4-ccda-4f2c-b4e2-bc18492fb3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e2e2b525-1804-4b7e-97f9-44119c302060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e6490fb4-7594-4266-a110-b49efd99d53d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 82,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f668b00e-df8e-4ec6-99b8-8973c2707df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3a890eb5-3fde-422c-841f-74e4a73731cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 58,
                "y": 13
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cf35b9b8-f817-4c6f-8df9-5a4227e003e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "76567b2d-41cd-444f-b863-2c1e7f6ee663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 116,
                "y": 35
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "db02670b-5513-4a37-a81a-4fd6bbc2aff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "06537b0e-8fa0-4e19-aeb3-1fecbad8821c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 13
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "228be0ad-7bfd-4aa1-a227-6d3e9bec81a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 35
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9ebb9739-03d6-4b82-bfbc-2729b87ec9b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 80,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "db7e52a8-5394-4d29-98bb-085c91011668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 86,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a5fc3b9f-b6b2-4821-a4a3-9b1dfc352ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "581cdc06-cad8-4d8a-9e61-471c263ea968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 100,
                "y": 13
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "879d2075-c068-4b3d-8ff1-79efc85ae53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "34bb4e70-ab07-4be4-9f5f-c2a7de0eb444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 42,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6091ec85-0764-41cc-a551-231cbb34f526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 110,
                "y": 35
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "866fe44e-f3f3-4b4a-b140-cd7d725844f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 68,
                "y": 35
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d11651ab-ae71-439b-8372-260d62feffeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 44,
                "y": 35
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "97dc5ae6-38f1-4c1b-84ad-2cf0943b3bbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 37,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6562c709-02b7-49e8-8c63-70872f22d33c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 44,
                "y": 13
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ef81a537-d6f7-402b-b57c-a6e820521253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 13
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b0130155-4d79-416d-afc3-097af29f3e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 35
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "16669522-9646-4517-9eb6-0b0787ee5c8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 35
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "69ec7fef-616d-48cd-bf7e-12be81d52ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b3c3b213-fe09-4335-ae00-b67cfa7e9a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 32,
                "y": 35
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8b5bbf3d-c969-4fa5-8e72-ea02ecea95c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 38,
                "y": 35
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1fedabaa-2420-47ad-8a96-458d75b6e02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 50,
                "y": 35
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7f8acffb-9c86-4821-88d6-e26c657a73cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ee3639c6-9644-467a-9a78-cdfe11b44915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 62,
                "y": 35
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e68c383a-826c-4eb0-b775-8647313495b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 35
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "840a90cc-bfaa-4e25-9391-ddd1b627b5bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cdeab7b9-cd15-46d8-a486-56363f1f49ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 35
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f5944bb0-b121-4962-81d9-cc874d869d4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dd3553c7-1696-48dd-8213-091051a90ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 92,
                "y": 35
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d3fcbc84-3feb-4f14-bc7a-0cf77c4754a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e140a5c9-ead1-4410-95e3-7e0ad224eb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 104,
                "y": 35
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c66f4afd-eac4-43e3-bceb-18e95722cc41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9315b220-d773-4701-92f5-09ecfca3b11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ff8f78a2-0906-4346-a1c8-5cec3693a924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 46
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f061cc70-8511-4481-96eb-b4e02b7209f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "03702065-2d44-4425-93b3-f83fa96474cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "27850412-626e-4e92-9c74-d63b23e9a4fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f33ec12e-3c16-4f6a-81d3-5fc439e6ae50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "12bca6cd-a7cf-485c-8625-bd6bd5a85579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "42b7d81e-fe0a-48df-8b2a-a3201a710137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2516797a-0062-426b-b77f-1c5264732dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 116,
                "y": 24
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8e346b04-ade3-4f8b-83da-1ad26b5d0586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 77,
                "y": 46
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "85a05ed0-e7d7-49a7-bf49-2130c9d7b1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 106,
                "y": 13
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1d99c09b-15b4-4238-9f4b-fcfd4fe7f5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 46
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9fb4c3dc-cbe2-450c-a858-4dee7d768da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 112,
                "y": 13
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "39d04f13-57f4-446b-ad47-f80fcacbace1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5e0dbbf6-5ea4-4156-a2e5-c5f15dcbf693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d3bd9fc2-b9e3-46fc-9f11-43c614cabf77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 13
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "63233628-85e1-4f31-83b8-abd74cc1e4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4f981851-6fbf-4be5-9ea7-88943e562453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 24
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ffbdf5b1-e6ed-45e6-b4c0-621dc2dd77b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a0562ee3-1f0c-465c-b12e-e85ca14be7fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "76e99b56-3cbe-4740-add0-f452492469dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6a943194-064e-460d-8b38-371247ffeeb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "32f51e80-6d84-4dff-994f-5822a42a8976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 32,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fc9d885a-a77f-4e00-ac10-7be51944bdfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 110,
                "y": 24
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8e4af65c-99a4-48b5-be41-f8068a307de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 38,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "18a6dfc5-ffd2-46d6-8c8c-f4f895a4533c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "019e8a53-1f49-4324-ab09-74e55d3fac75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 50,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3a1ed8e6-3fb8-4068-a5de-98fc69a9392b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c6caf642-7048-47fd-ac0a-a51a2a550298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "839328d0-da57-413b-9ce6-6eacae8c96e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "291b2615-eb2d-4785-ae12-380532ec59ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 62,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "15311b5d-97ee-40d7-a66e-db7ea43ed0a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 68,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "07458220-46dc-467b-956c-2e559b59d387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "be70e57e-a1d4-487b-a166-dacc544dbc45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "fcd26fbc-6ae0-4195-8081-cfc0690f3263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 80,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8f9d9efd-6329-4a46-bb27-e947fb91fb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "346c1f48-11ee-4617-9e3b-330415f43cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 13
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "34a6afd7-a8c5-40c4-821d-2a29884bace9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "155528e1-e91a-4364-ada1-5520f8be24d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cd9f7cb7-ce33-4c58-9b6a-d6bcc10cc78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 30,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a716fbf6-91ed-4202-93e4-12c7a4026960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 98,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b56ff73b-c02e-42cf-8703-851883b3f7e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 104,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "37a7e8c9-afdf-47c8-8a7c-9c47d44e9c98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 94,
                "y": 46
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7c0dd0f3-1302-430f-8a0f-52898d893d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 98,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0a7f243f-fd68-48fa-8685-0e979e4442f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 51,
                "y": 13
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}