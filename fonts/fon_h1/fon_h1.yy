{
    "id": "19387e7f-c5f1-4f29-be23-bec04d8cbc3a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fon_h1",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bbc04690-3c1d-4518-99f2-ec72f4d06b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e9c6f441-11aa-45bf-9316-58b5179052f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 6,
                "shift": 18,
                "w": 6,
                "x": 42,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2857185b-bf88-4981-9075-983c7c7152b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 399,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "db7ba23e-e19c-4978-bf44-c43dc8654143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "944d049f-b674-4cfc-a7d3-d7b8f5d9eb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 41
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ff31253b-f13e-43a2-adb4-de5fddd0e26d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "125c033c-5c30-4f49-a3bc-5a93b125a1de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2cd7ec0c-ea67-4ef7-bcb8-49268fe4ab9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 50,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3a123582-8a39-4b93-b7a4-b06789c7aa4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 427,
                "y": 80
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0be753d2-718f-4699-8174-8e0a094b6516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 440,
                "y": 80
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f450b401-882b-4bf7-9106-2866753211b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 245,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2abd6833-70e1-492d-8ef2-6cd4293423d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "829869f4-0a33-4894-ac1e-72850ccdbdc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9014e528-1a29-43a5-8991-aa2eae697a68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 466,
                "y": 80
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5b862d0d-2aa5-46e1-80f8-c4ce24ff0d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 24,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2dc9bf08-fe24-4be3-80f1-adf21869d93f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 405,
                "y": 41
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7b9daa3c-cd89-4116-8319-06f2f9a80c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "47f769da-608e-4d69-bfa4-26ad6bfbdc07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 456,
                "y": 41
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6cd9f4c2-7aec-4aa6-9a3d-e71590dd32a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 473,
                "y": 41
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1bbb980a-df46-461d-a9dd-9fae4eba05d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 165,
                "y": 80
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3a25025d-4355-4bd2-934d-bf7f325cd92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 380,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2ef45c8f-f6af-46ff-9028-ece74a411c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 133,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2c3a7a73-61b8-479e-99c4-6581ae2cb123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 41
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "34f6ad65-5a5e-4701-a1bb-787f0116dee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0f0447c9-8e0e-4e5e-8192-ce4f2f7d5c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 422,
                "y": 41
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a674c6fa-497d-4ebf-9758-278bbe24e0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 36,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5d7d7734-93ba-41e4-9a8d-f39159a86647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 33,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6c19844f-35ff-4955-8078-6ce4de0fbe36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 490,
                "y": 80
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "15cf4359-3d8f-4942-a3e3-220ddc07b768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 117,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7cbe2fe2-3c0c-4b96-ac47-28fbca2ad5b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 229,
                "y": 80
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "51e8206f-ae52-4eb9-9da1-3632125eaffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 369,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1f99ee3a-3a41-4427-9598-a94bae1105ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 413,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "589b61cf-7e48-48d3-82e3-6465e2737716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 263,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "97f0384e-cbf7-4306-8cb7-386af9a1198c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "039077d9-54ed-402b-bbda-0ea2862fc11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 490,
                "y": 41
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "258c556c-38f2-4d74-bcfb-fe5a70b9c658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 19,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "671402bc-8b68-4f16-93e6-b04825a72442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 74,
                "y": 41
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "80cf8919-443b-4abe-a6fc-79f4c2ed933d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 293,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "56c8e646-e908-45b7-aeac-212fa54a3a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 384,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bc8717b8-25cf-4c0e-83e3-ad9b9e5dd7cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "39168444-efde-4e59-887b-915ae61fc350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 41
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7040fcf8-5150-46c5-b57b-3f19cd29fb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 277,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4fd99fad-0bc4-4032-a3be-9cc49df0daf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 354,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1dd0a97e-e65e-4c74-b6d8-a2105f268f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "667f02b4-079b-44c8-8870-afb49fcc3cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 339,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8621b41c-43f6-42f8-abd7-1d74bba91d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "38085a1c-f0d8-484a-bd30-248388d9b704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 41
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c224f815-eb33-4ab2-acb9-c5355064b9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 283,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "afac5d02-085b-4330-80f0-299305610da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 439,
                "y": 41
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0a3782d7-da38-4f47-983e-c976c91f648d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d3d3e9e8-0a84-44d3-a264-71d502812a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 235,
                "y": 41
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d1f2dcab-ae60-4c29-89bb-cf2559111fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 354,
                "y": 41
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "993c3e02-59b8-40a4-a0b7-f80647f82381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 493,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b15913e0-3942-421f-a69a-a2ae9d0f388b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 41
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7522a4da-b792-424b-9de2-290bd32c16f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9962c813-d37e-45e6-841b-34eb084b3c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c387610b-a2d9-43b9-b502-afcc160274b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "828374df-c04c-42d0-a637-09f3b3d2dd67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6d602d6c-c46c-418e-b90c-0c32524f6060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 286,
                "y": 41
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "75cf2f58-8239-4cfb-8314-aab5f672833f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 5,
                "shift": 18,
                "w": 9,
                "x": 13,
                "y": 119
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4aea7373-e5f5-4f06-b27d-244ea7750f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 56,
                "y": 41
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "62e25b22-3c1c-46d0-9439-215703e89462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 4,
                "shift": 18,
                "w": 9,
                "x": 479,
                "y": 80
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cfbce8de-856d-4aab-acaf-bc6517bd813a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e6453a77-7e5e-4ef2-b583-c03bfa08c478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0b04fbe7-0e8b-45fb-925b-b9f1fc582b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 11,
                "x": 453,
                "y": 80
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "06855d16-6d41-43eb-a41d-24a75ed1b62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 218,
                "y": 41
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "78b4203f-a8c3-45a5-86ff-ea103ea15ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 371,
                "y": 41
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "acfae245-d1ec-4c2a-aafc-580e276bb2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 261,
                "y": 80
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ec8b8d00-046e-44ec-ae91-92bb0fe1af4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 388,
                "y": 41
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3bcbfd33-25e5-4722-a7cc-9cfaf5d5c37d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 252,
                "y": 41
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d87a2d7c-662f-4fee-8928-be9e5f1ccefd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9c88f03f-dc5e-4492-a9a5-2be7d5146dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 128,
                "y": 41
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a8417f28-8ab4-481f-95e6-353f0d6e8b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 101,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5c522120-83d6-441b-8f84-5fef18ffd6ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 53,
                "y": 80
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f10d97a1-189a-46ff-ad75-854e6506cf6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 69,
                "y": 80
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fa0b9c0b-6aa7-420e-9fd9-3b9b16424fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 269,
                "y": 41
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7d323820-ab48-4c26-b817-89cd9f89a673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 85,
                "y": 80
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c2432055-3b1e-4059-8514-26a842485aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "28e54412-3f1c-4157-94e5-9925a86cb44e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 213,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "108209a5-f538-4439-8069-ccd7fa93e748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 41
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "59701c39-8e37-4744-b650-213f844960dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 303,
                "y": 41
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c5454015-2701-4a0e-ae6a-7073be125eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 320,
                "y": 41
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "68e6305b-1f05-47a9-b6f9-cdf97dfa413f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 15,
                "x": 337,
                "y": 41
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c943d26a-7684-49e1-a4ef-e14458753d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 149,
                "y": 80
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5876026d-5367-46d9-b484-e441f286fef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 182,
                "y": 41
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b76ed4a9-6213-4719-b532-3fbd46dfbcc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 181,
                "y": 80
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1ae3f5e0-65d1-4303-9f4c-9776c0f63643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c579fe93-2a2f-4b61-a530-dcbc0775240e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b909787b-c472-4be2-9bf9-bc23c8c1f68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fffee153-53ab-4781-a6cd-1ad20645435b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "186961c5-574e-4d98-9dea-1c1df6ad63c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 197,
                "y": 80
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c3e38058-d5e8-4837-84f5-faf2edd25321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 324,
                "y": 80
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "af27b74c-14b8-4fac-851c-7f26782ee992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 7,
                "shift": 18,
                "w": 4,
                "x": 57,
                "y": 119
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e07e1411-69e7-420d-89aa-8a00eb5e1b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 309,
                "y": 80
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2b9399a0-3446-46ea-9c7d-9a23e6eca79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 399,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}