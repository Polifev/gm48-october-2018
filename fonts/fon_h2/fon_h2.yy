{
    "id": "4f5e10b0-e70f-484b-ab9e-d9c63f381b4f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fon_h2",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b3ea52f9-d1f9-434b-bb5e-4a095ef15b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "38ebe481-2528-4c56-9d16-e69289eacc3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 92,
                "y": 54
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3a9de1e3-44e1-4d8a-817d-7c07c3ccb0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 87,
                "y": 54
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d4c8201e-e2a8-4c65-9f93-b45f822c36c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 41
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dea4451c-5dd9-4239-b377-101c5ef0c2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 41
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6dcaff2d-4dc6-4b75-965d-c16a279a70e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 41
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e1f30a83-2bbf-4c27-ac2e-f48fc6b692d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 41
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c7ab0d15-43ca-4b90-b94f-17f9fa59398d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 22,
                "y": 67
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "63d8f0a7-9a21-4a40-a34d-de0930020098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 12,
                "y": 67
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3924f668-ca62-44db-bbcd-ab13000a9c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 102,
                "y": 54
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "32296a45-e933-433c-b185-da6a01a095e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 41
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a20a6b11-d4f6-4982-8dd1-4ae36d94d8ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4a347162-19fb-47d7-b1b1-dbabb9d52da9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 107,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "35d39aee-05b7-4173-9e32-f6211c2054e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 97,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "10398c55-5507-4661-af70-488a391ff06b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 117,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "855cbf0c-e618-42c1-b847-ffa6873bd935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 41
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "59e87f82-4936-4694-aef4-940bc23440a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 41
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6b968192-3aba-46c8-879c-8188fe316761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 28
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "df2b168a-12ff-4664-b337-36a754233aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 28
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "54956491-1f52-4c59-9044-e6adeb917a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 28
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e537e6ed-c3db-43b1-83d5-192f1bbb5a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 28
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "26caaf33-bcfa-46e0-8812-f29331ee68bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 28
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cd3774ed-d246-4f3a-bbb0-fa6234cc97f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 28
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7f9ab409-cf7c-4924-9948-58de9ca79719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 28
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "55cfe611-ad27-4e17-850a-2697e5fe6884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2f4a016d-47be-4448-ae56-aa4c0b10b308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 41
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "52045bf0-dce2-4de6-8d79-eaa0f60505fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 7,
                "y": 67
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a983be33-ec1b-4322-bd95-ab40fb0f4799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 17,
                "y": 67
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "54f5a002-6cfb-4a0e-9310-a184f67525d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 54
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "54544fc8-501b-4f65-bbeb-41c63a342a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 54
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2a241198-b2af-4be3-ae3c-61ad47cffcfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 54
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d5647d40-ad63-4084-a19a-50184e345fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 75,
                "y": 54
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2f3eee7b-41af-4dae-b628-2dc1a3778f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 54
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "81dc3e65-bcfe-494c-8b86-d8d12fd68342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 41
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "75c4dd14-3540-4d29-a0d9-00a0ed0fbe9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2dcf8bf6-bc87-4af9-a71b-73e0710d600f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 41
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6ec2ed91-d640-45c5-8893-088e560ea235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 41
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "33e57f02-d369-4cdf-8996-f1acdaa0cf0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 41
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "82569245-f0f6-4042-b155-676f090a7000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 54
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8c5b3ced-7406-49ac-b533-5c41d6ce4b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 54
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8ca1ff77-bd27-4d46-92d9-e8bde0de97a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 41
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a17d2c4c-b32d-42a3-86aa-1aaea393f3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 41
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d9913983-e051-4c17-8720-eaabb0cf0a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 54
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6777ecc6-2631-459e-9d7f-49042f2aa719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 28
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "deeed972-99ad-4ff6-aa33-e9bb19efdd8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 81,
                "y": 54
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7d64f2bc-9bbf-41f8-b2ff-5ec4f6a30679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 15
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ae6b3901-c60a-405c-97d0-2a45b5de371b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 15
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ce27943d-e057-4f24-b005-b13f0bd202df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 28
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4b5af0da-2e41-413c-9111-06ba9978335c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 15
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "05eb9ce5-3991-4870-a827-6a4c67622d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0079cfc6-ac55-4dc1-9d2a-62f8776e4bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b5d6eec2-ec41-4012-887b-7b3c63185697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "984bc975-2ef8-4835-b866-ad6a53dc0ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "56e270b1-02da-4832-aa13-b2093dc2fb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "95cb2b77-dedd-45f9-9b2a-50bcc10c4644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "843fae14-52b6-46fa-8bc9-273bc42f5c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 15
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "06acf83d-555e-48b8-a457-5a35103c376a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b405fd4d-f8f7-4b59-9cc6-af3fc3986c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e378db0e-c410-477c-bb41-c71595750bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d7e63ab2-b84e-4890-8db8-672f258eaa90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9daa3a2d-a6ed-47bb-924c-e8c6c9010ea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8d793027-f5d0-4b50-b2d6-04040cc6cf71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 112,
                "y": 54
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6b76afbb-7be9-4b15-8dfe-1bb2d0c335ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5b2e6978-f8ca-4341-b69f-4c397b12f661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4d04b77c-fc98-4467-94c4-c35e9b801675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 67
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7b9329ed-55fc-4726-a196-73a749ab590d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c0ad1b03-acd9-41a9-a129-9624a12b462f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9fda569c-c812-4275-93d9-19328962acfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f44a62fd-a1dc-4650-aed9-87920a17e393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "33a62d3f-f9bf-4a4f-9287-97873a815ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 15
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "237993e7-670e-4ffb-9187-82e45d5a0ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 15
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8ad2ddba-f4e4-4964-9c31-78b317db9920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 15
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "68506956-2b0e-4152-af1f-3159fbb03fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d0340487-d4d4-471d-bb0b-9f8908d10149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 28
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2aefe502-491d-47b4-ae5d-6c3deed2638f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 57,
                "y": 54
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5e78dd39-bd7e-4a32-8098-2f03dd525b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cc9cccad-0b5e-407b-b344-605d14c26f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 28
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "31dcfd2f-b752-4a5d-9059-4cd8c15388da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6db768c7-7109-4c84-bb63-c070e3eb3142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 28
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e7813850-5c22-47ea-94fa-9380107d6ae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f33497b3-e8aa-4715-b6b8-cd529f27eb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 15
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "48fa285a-6409-4169-a06a-9eee96a83fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c5898f07-44bb-479e-b9c3-97e1da33dae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 15
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "05f879ad-1205-417b-8c32-af668df41e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 15
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3278f7f8-d66b-4f10-a23a-3344a2509251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 15
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "258572c1-31ae-42cc-896b-d46e261f3280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 15
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "da7bb53a-adad-4903-bed4-aae0d8953242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 15
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "38dfee92-e1c2-40ba-a6ed-2ffc3e61be91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 15
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6fd72930-9aa8-4189-98ea-1f14c8407c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 15
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3f196ad2-78b6-42b7-a4d3-ccd1b9347b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 15
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e3a31ce3-ccf5-4d20-9b72-e1f7caa0f97e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 15
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f445cb8a-2fff-41a4-bdce-541e4432c3f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 69,
                "y": 54
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cb8f3504-a779-4223-a9da-c3d94f72154e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 11,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 26,
                "y": 67
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5c87848a-2d2b-4767-863c-7778d6d73d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 54
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "826ae64a-204c-40ca-821c-5f96e3d59e1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 41
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 7,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}